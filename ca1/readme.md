# Class Assignment 1

## Part I
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783/src/main/

<ins>Repository structure</ins>: 
[FIRST ATTEMPT - up until tag ca1-part2] The Spring and React Tutorial structure has been copied to the root folder of the repository. 
Another folder has been added, named ca1, where you can find a copy of the content of the basic folder, as well as this
readme.md file and all the work done in the context of CA1.
[SECOND ATTEMPT - starting on tag ca2-projectStructureRefactoring]
Due to the execution of CA2, the whole project used in CA1 had to be moved from the root of the repository to its own folder, 
named ca1. A commit was done on 31st March to fix this issue. The content of the files of CA1 has not been changed, the refactor 
targeted only the location change of the project folders and files, as can be observed in the commit. This readme.md file 
was updated on the same day to justify the repository structure change. 

<ins>Entry note</ins>: Due to problems in the project structure, please consider for the effect of this assignment
the commits starting in 67909bb. The initial version is marked as v1.1.1, replacing v1.1.0.


Using the terminal, the following steps were followed to implement the exercise proposed.

1. 
   * Create the repository in Bibucket named devops-21-22-ATB–1211783
   * `cd /Users/martasilveira/Documents/"Switch 2021"/DevOps`
   * `mkdir DevOpsMVS`
   * `cd ./DevOpsMVS`
   * `git init`
   * Copy over the files from the Spring Tutorial to the folder above, except for the .git and .gitignore files
   * `git add . `
   * `git commit -a -m "Refactor repository structure, due to project module errors"`
   * `git remote add origin https://martasilveira5@bitbucket.org/martasilveira5/devops-21-22-atb-1211783.git`
   * `git push origin main`

>Due to changes in the repository structure shown above, the changes done to the project in the first class assignment 
> (0 - to include the JobTitle field) had been lost and were added back between exercises 1 and 2, please refer to commit 829308b.
> The git commands used to perform this corrective action were similar to the steps explained above. 

2. 
   * Copy the content of the basic folder to ca1 folder
   * `git add .`
   * `git commit -a -m "Create a folder for the class assignment"`
   * `git push origin main`
3. 
   * `git tag -a v1.1.1 -m "(...)"` ----> In this case the tag 1.1.1 was used instead of v1.1.0, because the tag 1.1.0 had already
been used with a commit that pointed to a corrupted project structure. All the steps until this point had to be repeated and 
therefore the tag v1.1.1 was used. 
   * `git push origin v1.1.1`
4. 
   * Edited the files DatabaseLoader.java, Employee.java and EmployeeTest to add the new JobYears attribute, validations and respective tests:
     * The parameter "startDate" was passed to the Employee constructor, because it makes more sense to dynamically calculate
the Employee Job years based on a starting date than storing a static value in the database. The method calculateYearsOfEmployee
was created for that purpose.
     * The methods equals, hashcode and toString had also to be updated to include the newly added fields. 
     * Get and set methods were added for the new attributes.
     * Updated the databaseLoader.java file with one more entry (Gandalf) and added the new necessary attributes for the existing one. 
     * Files app.js and bundle.js were edited to include the new column in the table "Years of Employee".
     ![img_3.png](basic/images/img_3.png)
     * Debugged the server to confirm that the frontend is showing the correct information:
     `./mvnw spring-boot:run` and then open the address `localhost:8080` on browser
       ![img_6.png](basic/images/img_6.png)
     * `git add .` 
     * `git commit -a -m "Made needed changes to DatabaseLoader.java, Employee.java, app.js and bundle.js to include the new employee attribute: yearsOfEmployee. Added info to the readme file. Addresses issue #1"`
     * `git push origin main`
     
     * The parameters passed to the Employee constructor had to be validated, considering most of them were Strings and none of them
       could be empty, the method isAValidString was created. This checks that the String is neither null, nor empty, nor equal to a space. For the
       employee start date, the only check in place is to confirm that the date is not null.
     * All the set methods were updated to include the validations mentioned in the previous point. 
     * 31 tests were added to ensure full coverage.
     * Confirmed all 31 tests are passing:
       ![img_1.png](basic/images/img_1.png)
     * It was also tested how the app would react when adding an Employee entry to the DatabaseLoader with an empty field. In this case, Spring did not run
successfully and threw an exception, as coded in the Employee constructor:
   
![img_4.png](basic/images/img_4.png)
     

   ![img_5.png](basic/images/img_5.png)
   * `git add .`
   * `git commit -a -m "Change databaseLoader.java, Employee.java, bundle.js to include the new field employeeYears and the necessary validations to Employee construtor, as well as tests for the whole class. Addresses issue #1"`
   * `git push origin main`
   * `git tag -a v1.2.0 -m "(...)"` ----> Add a message
   * `git push origin v1.2.0` 
   
>Some commits followed in this phase to write down this same document readme.md and to refactor the calculateYearsOfEmployee method.

5.
   * `git tag -a ca1-part1. -m "(...)"` ----> Add a message
   * `git push origin ca1-part1.`


## Part II
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783/src/main/

1. Note that the default branch is named "main" instead of "master" and it will be used to "publish" the "stable" versions.

2. Firstly a new branch was created and the HEAD was moved to that branch. 
* `git branch email-field`
* `git checkout email-field`
* The following changes were done to include the new field Email Address:
  * DatabaseLoader.java file was edited to include an email address in each register.
  * Employee.java class was edited to include the new attribute, add it to the constructor, equals, hashcode and toString.
  and add new get and set methods for this attribute. 
  * It was also implemented a validation method for this new attribute that ensures the email has a format that matches a standard 
Regex format: ^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$. This validation is called both in the constructor and 
set method. 
  * Test methods were added to verify the implementation of the previous two points: setInvalidEmail(), getEmailAddress(),
setEmailAddress(), testEmptyEmail(), testInvalidEmail().
  * The files app.js and bundle.js were edited to include a new column named "Email Address".
* Tested front end: ´localhost:8080 on browser`
  * `./mvnwspring-boot:run`
  ![img_2.png](basic/images/img_2.png)
* Made sure all tests are passing:
![img_1.png](basic/images/img_1.png)
* Tried to add an invalid email to the database (via DatabaseLoader.java) and verified that Spring is throwing an exception 
and exiting:
![](basic/images/Screenshot%202022-03-24%20at%2016.35.05.png)

* `git add .`
* `git commit -a -m "Resolve issue #5. Added new field emailAddress, required changes on databaseloader.java, 
Emploee.java, app.js, bundle.js, EmploeeTest.java. Moved readme.md images to folder images."`
* Went back to the main branch to merge next: `git checkout main`
* `git log --oneline --decorate --graph --all`
* Made a forward merge `git merge email-field`
* `git push origin email-field`
* `git push origin main`
* Added a tag: `git tag v1.3.0` and then `git push origin v1.3.0`

> Note that in this case the push was only done after the merge with the main branch.
> Besides, no changes were done to the main branch between the commit and the merge. This means that a "fast-forward"
> merge was done and that the new branch will look like a continuous line in Bitbucket. Only when selecting the option 
> all branches, as shown below, it is possible to visualize the newly crated branch:

![](basic/images/Screenshot%202022-03-24%20at%2017.05.23.png)

3. Firstly a new branch was created and the HEAD was moved to that branch.
* `git branch fix-invalid-email`
* `git checkout fix-invalid-email`

> Note that the changes needed to ensure that the email is valid, i.e. that it has a "@", had already been implemented 
> in the first branch. Hence, this new branch will only be created for the purpose of demonstration and
> only a minor change was done to the DataBaseLoader.java: added a new entry, as shown below:

![](basic/images/img_24.png)

* `git add . `
* `git commit -a -m "Resolve issue #6. Add one more entry to the DB"`
* `git push origin fix-invalid-email`

* A push was done to the remote repository right after the commit, so this time it is possible to clearly check 
the creation of a new branch:

![](basic/images/img_25.png)
![](basic/images/img_26.png)

* Went back to the main branch: `git checkout main`
* Merged the main branch with the new branch: `git merge fix-invalid-email`
* Again it was verified that a fast-forward merge has been done with the main branch:
![](basic/images/img_27.png)
* Verified that we are back at the main branch `git branch`
![](basic/images/img_28.png)
* Push the changes to the remote repository `git push origin main`

* Added a tag:
* `git tag v1.3.1`
* `git push origin v1.3.1`

>Some commits followed in this phase to write down this same document readme.md.

4. Added a final tag for part II:
    * `git tag -a ca1-part2 -m "(...)"` ----> Added a message
    * `git push origin ca1-part2`



________________________________________
# Alternative - Mercurial & Perforce

## Part I
###Alternative Implementation

Professor has been added as a collaborator of the HelixTeamHub repository. So, he should be able to check the changes
done there.
<ins>Link to the repository</ins>: https://helixteamhub.cloud/restless-canvas-9100/projects/mercurialtest/repositories/DevOpsMVSMercurial

Using the terminal, the following steps were followed to implement the exercise proposed.

1. 
   * `cd /Users/martasilveira/Documents/"Switch 2021"/DevOps`
   * `mkdir DevOpsMVSMercurial`
   * `cd ./DevOpsMVSMercurial`
   * `hg init`
   * Copied over the files from the Spring Tutorial to the folder above, except for the .git and .gitignore files 
   * `hg add`
   * `hg commit -m "Initial commit"`
   * Created the repository in Perforce named DevOpsMVSMercurial 
   * `hg push https://1211783isepipppt@helixteamhub.cloud/restless-canvas-9100/projects/mercurialtest/repositories/mercurial/DevOpsMVSMercurial`
   * `mkdir ca1`
   * Copied the basic folder to ca1 folder 
   * Copied the main pom.xml file to the ca1 folder
2. 
   * `hg add` 
   * `hg commit -m "Created ca1 folder and copied over the basic folder to this fodler."`
   * Edited the hgrc file to define the default push path:
      1. [paths]
         default = https://1211783isepipppt@helixteamhub.cloud/restless-canvas-9100/projects/mercurialtest/repositories/mercurial/DevOpsMVSMercurial
   * `hg push default`
3. 
   * `hg tag v1.1.0`
   * `hg push`
4. Edited the files DatabaseLoader.java, Employee.java and EmployeeTest to add the new JobTitle and JobYears attributes, validations and respective tests. Note that the 
code implementation and its justifications are exactly the same as the ones mentioned in the main exercise using GIT. 
   2. Confirmed all tests are passing:
      ![img_1.png](basic/images/img_1.png)
   * `hg add`
   * `hg commit -m "Made needed changes to Employee.java, EmployeeTest.java and DatabaseLoader.java to reflect the request of adding two new fields: jobTitle and JobYears. Addresses issues #1 and #2"`
   * `hg push default`
   * Edited the files bundle.js, app.js to reflect the previous changes in the frontEnd. 
   * Confirmed that the frontend is showing the correct information: `localhost:8080 on browser`
   ![img_6.png](basic/images/img_6.png)
   * It was also tested how the app would react when adding an Employee entry to the DatabaseLoader with an empty field. In this case, Spring did not run
       successfully and threw an exception, as coded in the Employee constructor:

![img_5.png](basic/images/img_5.png)
   * `hg add`
   * `hg commit -m "Edited the files bundle.js, app.js to reflect the previous changes in the frontEnd. Addresses issues #1 and #2"`
   * `hg push default`
   * `hg tag v1.2.0`
   * `hg push default`
6. 
   * `hg tag ca1-part1`
   * `hg push default`

>Some commits followed in this phase to refactor the calculateYearsOfEmployee method.


###Alternative Analysis
##### How does Mercurial compare to Git regarding version control features?
#### Non-technical aspects
  * The first impact when trying to use Mercurial is the limited range of options of remote hosts. Mercurial official
website (https://www.mercurial-scm.org/wiki/MercurialHosting) provides a list of compatible remote hosts, however it 
doesn't comprise hosts like Bitbucket (dropped in 2020) or GitHub, which are two of the currently most used VCS hosts 
(refer to https://www.g2.com/categories/version-control-hosting). 
  * For the purpose of this exercise, the host chosen was Perforce. How does it compare with Bitbucket? **Perforce vs Bitbucket**
1. It revealed to be quite simple to use 
2. History of commits doesn't seem to be as easy to interpret as when using Bitbucket
3. It is difficult to visualize tags, which are stored only in a text file (.hgtags), in association with the commit ID 
4. Issues are organized by Milestones 
5. It is not possible to resolve issues using the mercurial commit message 

* As a consequence of Git's popularity, its online community support (websites such as Stackoverflow) is significantly 
* broader than Mercurial's. This is a relevant aspect that makes the usage of Git friendlier. 

#### Technical aspects
(Main reference - Mercurial's official website: https://www.mercurial-scm.org/wiki/GitConcepts#Git.27s_staging_area)
* Both Mercurial and Git are DVCS (distributed version control systems).
* Git supports the concept of a staging area. Staging is the practice of adding files to the next commit. This is useful
when not all files with changes are to be committed. In contrast with Git, Mercurial does not expose a staging area, 
this is explained in Mercurial's official website:
>Git is the only DVCS (distributed version control system) that exposes the concept of index or staging area. 
>The others may implement and hide it, but in no other case is the user aware or has to deal with it. Mercurial's rough
>equivalent is the DirState, which controls working copy status information to determine the files to be included in the 
>next commit. But in any case, this file is handled automatically. Additionally, it is possible to be more selective 
>at commit time either by specifying the files you want to commit on the command line or by using hg commit --interactive.

* Even though Mercurial supports tags, its concept is different than Git's, as explained in Mercurial's website:
>Global tags is one of the aspects that really differs from Git. Apparently they serve the same purpose, however 
> they are treated differently. In Git, global tags have a dedicated repository object type; these tags are usually 
> referred as annotated tags. In Mercurial, though, they are stored in a special text file called **.hgtags** residing
> in the root directory of a repository clone. Because the .hgtags file is versioned as a normal file, all the 
> file modifications are stored as part of the repository history.

* Storage Type and empiric data regarding speed performance

Refer to https://keithp.com/blogs/Repository_Formats_Matter/ and "An analysis on Version Control Systems", 
in "Conference: 2020 International Conference on Emerging Trends in Information Technology and Engineering (ic-ETITE)", page 8.
As opposed to Git, where each commit stores a snapshot of the whole project, Mercurial uses a forward delta storage type.
>Mercurial uses a truncated forward delta scheme where file revisions are appended to the repository file, 
> as a string of deltas with occasional complete copies of the file (to provide a time bound on operations). 
> This suffers from two possible problems — the first is fairly obvious where corrupted writes of new revisions can 
> affect old revisions of the file. The second is more subtle -- system failure during commit will leave the file 
> contents half written. 

Although it was not found any trustworthy reference that points to lower speed performance in VCS using a storage type based on 
forward delta scheme, empiric data seems to suggest that Git attains a much better performance in terms of pushing and commiting
operations. 
While using Mercurial, as shown below, the first push took almost 3 minutes for a 435,7 MB project. On the other hand, Git didn't take more than a handful of seconds.
![img.png](basic/images/img.png)

Erik Sink (https://ericsink.com/vcbe/html/dvcs_performance.html#table_cvcs_dvcs_perf) reports on his blog 
some (not so significant) speed performance differences between Mercurial and Git while committing:
![img_8.png](basic/images/img_8.png)

##### How can Mercurial be used to solve the same goals as presented for this assignment?
* As can be noted in the "Alternative implementation" section of this first part of the exercise, it was clear that Mercurial
fully supports the goals of this assignment. In fact, there are very subtle differences between the commands and sequence 
of commands used in Git and in Mercurial.
* Mercurial's official website provides a command equivalence table, shown below, that confirms the previous statement:
![img_7.png](basic/images/img_7.png)
  (this is just an excerpt of the original table, showing the relevant commands for this first part of the exercise)


## Part II
###Alternative Implementation
<ins>Link to the repository</ins>: https://helixteamhub.cloud/restless-canvas-9100/projects/mercurialtest/repositories/DevOpsMVSMercurial/tree/default

1. Note that the main branch is named "default" instead of "master" and that it will be used to "publish" the "stable" versions. 
The steps following next were based on guidelines found on Mercurial's official website:
   https://www.mercurial-scm.org/wiki/NamedBranches
   https://www.mercurial-scm.org/wiki/Branch
   https://www.mercurial-scm.org/wiki/Merge

2. Firstly a new branch was created and the HEAD was moved to that branch.
* `hg branch email-field`
* To verify that we are currently on the new branch: `hg branch`
![](basic/images/img_29.png)

* The following changes were done to include the new field Email Address:
    * DatabaseLoader.java file was edited to include an email address in each register.
    * Employee.java class was edited to include the new attribute, to add it to the constructor, equals, hashcode and toString.
      New get and set methods for this attribute were also added.
    * Test methods were added to verify the implementation of the previous two points: getEmailAddress(),
      setEmailAddress(), testEmptyEmail().
    * The files app.js and bundle.js were edited to include a new column named "Email Address".
* Tested front end:
    * `./mvnwspring-boot:run`
    * `localhost:8080 on browser`
      ![](basic/images/img_30.png)
* Made sure all tests are passing:
  ![](basic/images/img_32.png)
* Tried to add an empty email to the database (via DatabaseLoader.java) and verified that Spring is throwing an exception
  and exiting:
![](basic/images/img_33.png)

* `hg status`
* `hg commit -m "Addresses issue #3, add support for email-field on new branch"`
* Mercurial requires a special push command to reflect the new branch on the remote repository:
* `hg push --new-branch`
![](basic/images/img_34.png)
* When using the command `hg parents` it is possible to verify that the latest changeset (commit) is associated 
with the newly created branch:
![](basic/images/img_35.png)

* Went back to the main branch to merge next: `hg up default`
* `hg branches` lists the currently available branches:
![](basic/images/img_36.png)

* Made a merge `hg merge email-field`
![](basic/images/img_37.png)

* As requested, the merge was committed and push to the remote repository.
* `hg commit -m "Merge branches, addresses issue #3"`
* `hg push default`
* `hg branches` --> the default is now ahead of "email-field" branch
![](basic/images/img_38.png)

* Added a tag: `hg tag v1.3.0` and then `hg push`

> No changes were done to the main branch between the first push and the merge. This means that a "fast-forward"
> merge was done.

3. Firstly a new branch was created and the HEAD was moved to that branch.
* `hg branch fix-invalid-email`
* `hg commit -m "Addresses issue #4, created new branch fix-invalid-email"`
* `hg push --new-branch`
* It was implemented a validation method which ensures that the email has a format that matches a standard
Regex format: ^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$. This validation is called both in the constructor and
set method. 
* Tests were implemented to verify this new validation was working correctly:
setInvalidEmail(), testInvalidEmail()
![](basic/images/img_39.png)
* It was also verified that Spring doesn't run when an email without "@" is entered
in the database:
![](basic/images/img_40.png)
![](basic/images/img_41.png)
* Frontend was tested: `localhost:8080 on browser`
![](basic/images/img_42.png)

* `hg commit -m "Address issue #4, add support for email validation to class Employee.java and respective tests"`
* `hg push`
![](basic/images/img_43.png)


* Went back to the main branch: `hg update default`. As it can be seen, default is still behind fix-invalid-email:
![](basic/images/img_44.png)

* Merged the main branch with the new branch: `hg merge fix-invalid-email`
* `hg commit -m "Address issue #4, merge with default with branch fix-invalid-email"`
* `hg push`
![](basic/images/img_45.png)

* Added a tag:`hg tag v1.3.1`
* `hg push`


4. Added a final tag for part II:
* `hg tag ca1-part2`
* `hg push`


###Alternative Analysis
##### How can Mercurial be used to solve the same goals as presented for this assignment?
To better understand the upcoming information, it is important to understand what changesets are:

**Changesets** (as explained in https://book.mercurial-scm.org/read/tour-basic.html)
_This field has the format of a number, followed by a colon, followed by a hexadecimal (or hex) string. These are identifiers for the changeset. 
The hex string is a unique identifier: the same hex string will always refer to the same changeset in every copy of this repository.
(...) In Mercurial terminology, we call each stored 'snapshot' of history a changeset, because it can contain a record of changes to several files.
(...) We can use the hg commit command for this purpose._ 

As explained in the article below, there are many ways of implementing branches with Mercurial. The article presents 4 different options,
each of them with some pros and cons:
https://stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/
* Branching with Clones - this options has several disadvantages, the heaviest one being the slow speed and exaggerated redundancy.
* Branching with Bookmarks - even though this is a lightweight solution and in some extent similar to how Git operates branches,
  it is limited to local repositories,
  which makes it not suitable for the purpose of this exercise.
* Branching Anonymously - Using anonymous branching obviously means that there won't be any descriptive name for a branch,
  this method is great for quick-fix, two-or-three-changeset branches.
* **Branching with Named Branches** - According to Steve Losh (https://stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/), the branch name is 
permanently recorded as part of the changeset's metadata. Different perspectives argue that this can be seen as an advantage or as a disadvantage. 
Either way, it indeed favours graphic visualization of commit history. 

The commands used to implement Named Branches with Mercurial are mostly similar to Git's, as shown below and observed in the 
previous section (exercise implementation):
![](basic/images/img_47.png)
(source: http://jgrasstechtips.blogspot.com/2011/03/mercurial-and-git-cheatsheet.html)

For example, the command `hg up (...insert branch name...)` was used to place the user
in the tip changeset of that named branch (similarly to `git checkout` command). There is also a specific
push command for branches, which has also been used above: `hg push --new-branch`

In a nutshell, the experience of implementing branches in Git and Mercurial is quite similar, yet the way each DVCS operates is different. 
The main differences will be presented next.

##### How does Mercurial compare to Git regarding version control features?
 (source: https://www.perforce.com/blog/vcs/mercurial-vs-git-how-are-they-different)
In this article, it is argued that the main difference between Mercurial and Git is the **branching structure**.  
_In Git, branches are only references to a certain commit. (...) Branching in Mercurial doesn't share the same meaning.
 (...) In Mercurial, branches (Named Branches) refer to a linear line of consecutive changesets. (...) 
Mercurial embeds the branches in the commits (changesets), where they are stored forever._
 
Named branches allow a workflow where it is possible to switch back and forth between branches in the same working directory. 
This requires additional care not to push code to the wrong branch, since it's quite easy to have more than one branch 
with the same name.
As opposed to Mercurial, this is something that can be easily avoided with git. Git Branch information is just a head, 
it is never stored as part of a git changeset's metadata. It actually moves the pointer to the new changeset, allowing 
to create, delete and change a branch anytime, without affecting the commits.
This is where the main difference lies, it is very unlikely to push code to the wrong place.

(source: https://stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/)
Besides the different branching structure, Mercurial **pushes/pulls all branches by default**, while git pushes/pulls only the current branch.
To push/pull only a single branch with Mercurial it is possible to use the --rev option.






