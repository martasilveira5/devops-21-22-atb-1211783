package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;


class EmployeeTest {

    @Test
    void testEqualEmployeeIsEqual() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );
        Employee newEmployee1 = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );

        boolean result = newEmployee.equals(newEmployee1);

        assertTrue(result);
    }

    @Test
    void testEqualEmployeeHasSameHashCode() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );
        Employee newEmployee1 = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );

        assertEquals(newEmployee1.hashCode(), newEmployee.hashCode());
    }

    @Test
    void ensureEmployeeWithDifferentContentIsNotEqual() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );
        Employee newEmployee1 = new Employee("Gandalf", "Grey", "you shall not pass", "Magician", LocalDate.of(1900,1,1), "gandalf@isep.ipp.pt");

        boolean result = newEmployee.equals(newEmployee1);

        assertFalse(result);

    }

    @Test
    void ensureEmployeeWithDifferentContentHasDifferentHashcode(){
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );
        Employee newEmployee1 = new Employee("Gandalf", "Grey", "you shall not pass", "Magician", LocalDate.of(1900,1,1), "gandalf@isep.ipp.pt");

        assertNotEquals(newEmployee.hashCode(), newEmployee1.hashCode());
    }

    @Test
    void testEqualEmployeeWithItself() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );

        boolean result = newEmployee.equals(newEmployee);

        assertTrue(result);
    }

    @Test
    void testEmployeeNotEqualToNull() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );

        boolean result = newEmployee.equals(null);

        assertFalse(result);
    }

    @Test
    void testEmployeeNotEqualToOtherObject() {
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", LocalDate.of(1990,1,1), "frodo@isep.ipp.pt" );

        boolean result = newEmployee.equals(new Object());

        assertFalse(result);
    }

    @Test
    void testEmptyFirstName(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("", "Baggins",
                "ring bearer", "On a Mission", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt"));

    }

    @Test
    void testEmptyLastName(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "",
                "ring bearer", "On a Mission", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt"));

    }

    @Test
    void testEmptyDescription(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "", "On a Mission", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt"));

    }

    @Test
    void testEmptyJobTitle(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", " ", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt"));

    }

    @Test
    void testEmptyStartDate(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "On a Mission", null, "frodo@isep.ipp.pt"));

    }

    @Test
    void testEmptyEmail(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "On a Mission", LocalDate.of(1990, 1, 1), ""));

    }

    @Test
    void testInvalidEmail(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "On a Mission", LocalDate.of(1990, 1, 1), "sdfsfes"));

    }

    @Test
    void calculateEmployeeYears(){
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        int expected = LocalDate.now().getYear()-startDate.getYear();
        int result = newEmployee.getYearsOfEmployee();

        assertEquals(expected, result);
    }


    @Test
    void getId() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        Long result = newEmployee.getId();

        assertNull(result);
    }

    @Test
    void setId() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setId((long) 1);

        assertEquals(1, newEmployee.getId());
    }


    @Test
    void getFirstName() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        String expected = "Frodo";
        String actual = newEmployee.getFirstName();

        assertEquals(expected, actual);
    }

    @Test
    void setFirstName() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setFirstName("Gandalf");

        assertEquals("Gandalf", newEmployee.getFirstName());
    }

    @Test
    void setInvalidFirstName(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setFirstName("");

        String expected = "Frodo";
        String actual = newEmployee.getFirstName();

        assertEquals(expected, actual);
    }

    @Test
    void getLastName() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        String expected = "Baggins";
        String actual = newEmployee.getLastName();

        assertEquals(expected, actual);
    }

    @Test
    void setLastName() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setLastName("Grey");

        assertEquals("Grey", newEmployee.getLastName());
    }

    @Test
    void setInvalidLastName(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setLastName("");

        String expected = "Baggins";
        String actual = newEmployee.getLastName();

        assertEquals(expected, actual);
    }

    @Test
    void getDescription() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        String expected = "ring bearer";
        String actual = newEmployee.getDescription();

        assertEquals(expected, actual);
    }

    @Test
    void setDescription() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setDescription("This is a description");

        assertEquals("This is a description", newEmployee.getDescription());
    }

    @Test
    void setInvalidDescription(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setDescription("");

        String expected = "ring bearer";
        String actual = newEmployee.getDescription();

        assertEquals(expected, actual);
    }

    @Test
    void getJobTitle() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        String expected = "On a Mission";
        String actual = newEmployee.getJobTitle();

        assertEquals(expected, actual);
    }

    @Test
    void setJobTitle() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setJobTitle("This is a test");

        assertEquals("This is a test", newEmployee.getJobTitle());
    }

    @Test
    void setInvalidJobTitle(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setJobTitle("");

        String expected = "This is a test";
        String actual = newEmployee.getJobTitle();

        assertEquals(expected, actual);
    }

    @Test
    void getYearsOfEmployee() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        int expected = 32;
        int actual = newEmployee.getYearsOfEmployee();

        assertEquals(expected, actual);
    }

    @Test
    void getYearsOfEmployeeStartDateFuture() {
        LocalDate startDate = LocalDate.now().plusDays(400);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        int expected = 0;
        int actual = newEmployee.getYearsOfEmployee();

        assertEquals(expected, actual);
    }

    @Test
    void setYearsOfEmployee() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setYearsOfEmployee(199);

        assertEquals(199, newEmployee.getYearsOfEmployee());
    }

    @Test
    void setInvalidYears(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setYearsOfEmployee(-100);

        int expected = LocalDate.now().getYear() - 1990;
        int actual = newEmployee.getYearsOfEmployee();

        assertEquals(expected, actual);
    }

    @Test
    void setEmailAddress() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        newEmployee.setEmailAddress("newFrodo@isep.ipp.pt");

        assertEquals("newFrodo@isep.ipp.pt", newEmployee.getEmailAddress());
    }

    @Test
    void getEmailAddress(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");

        String expected = "frodo@isep.ipp.pt";
        String actual = newEmployee.getEmailAddress();

        assertEquals(expected, actual);
    }

    @Test
    void setInvalidEmail(){
        Employee newEmployee = new Employee("Frodo", "Baggins",
                "ring bearer", "This is a test", LocalDate.of(1990, 1, 1), "frodo@isep.ipp.pt");
        newEmployee.setEmailAddress("eewewqerew.isep.");

        String expected = "frodo@isep.ipp.pt";
        String actual = newEmployee.getEmailAddress();

        assertEquals(expected, actual);
    }

    @Test
    void testToString() {
        LocalDate startDate = LocalDate.of(1990,1,1);
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "On a Mission", startDate, "frodo@isep.ipp.pt");

        String expected = "Employee{" +
                "id=null" +
                ", firstName='Frodo" + '\'' +
                ", lastName='Baggins" + '\'' +
                ", description='ring bearer" + '\'' +
                ", jobTitle='On a Mission" + '\'' +
                ", yearsOfEmployee='32" + '\'' +
                ", emailAddress='frodo@isep.ipp.pt" + '\'' +
                '}';

        String result = newEmployee.toString();

        assertEquals(expected, result);
    }
}