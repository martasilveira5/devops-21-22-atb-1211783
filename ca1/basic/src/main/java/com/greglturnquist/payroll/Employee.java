/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.time.LocalDate;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int yearsOfEmployee;
	private String emailAddress;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, LocalDate startDate, String emailAddress) {

		if(isAValidString(firstName) && isAValidString(lastName) && isAValidString(description)
				&& isAValidString(jobTitle) && isAValidString(emailAddress) && isValidEmail(emailAddress) && startDate!=null) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.description = description;
			this.jobTitle = jobTitle;
			this.yearsOfEmployee = calculateYearsOfEmployee(startDate);
			this.emailAddress = emailAddress;
		}
		else { throw new IllegalArgumentException("Please enter valid data");}
	}


	/**
	 * This method uses the date when an Employee starts to work for the company and calculates the
	 * different between that year and the current year.
	 * @param startDate
	 * @return number of years that the employee has been working in the company
	 */
	private int calculateYearsOfEmployee (LocalDate startDate){
		int result = 0;
		if(!startDate.isAfter(LocalDate.now())){
			result = LocalDate.now().getYear() - startDate.getYear();
		}
		return result;
	}

	/**
	 * This method verifies that the String passed as argument is not empty nor equal to a space,
	 * thus making it valid for usage in the constructor of the Employee object.
	 * @param stringToTest
	 */
	private boolean isAValidString (String stringToTest){
		return stringToTest!=null && !stringToTest.isEmpty() && !stringToTest.equals(" ");
	}

	/**
	 * This method verifies that the emailAddress passed as an argument is a valid one.
	 * @param emailAddress
	 * @return true/false
	 */
	private boolean isValidEmail(String emailAddress) {
		String regexPattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
		return Pattern.compile(regexPattern)
				.matcher(emailAddress)
				.matches();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
				Objects.equals(jobTitle, employee.jobTitle) &&
				Objects.equals(yearsOfEmployee, employee.yearsOfEmployee) &&
				Objects.equals(emailAddress, employee.emailAddress);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, jobTitle, yearsOfEmployee, emailAddress);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (isAValidString(firstName)){
			this.firstName = firstName;
		}
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (isAValidString(lastName)) {
			this.lastName = lastName;
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (isAValidString(description)){
			this.description = description;
		}
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String newJobTitle) {
		if (isAValidString(newJobTitle)){
			this.jobTitle = newJobTitle;
		}
	}

	public int getYearsOfEmployee() {
		return yearsOfEmployee;
	}

	public void setYearsOfEmployee(int years) {
		if (years > 0) {
			this.yearsOfEmployee = years;
		}
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		if (isAValidString(emailAddress) && isValidEmail(emailAddress)){
			this.emailAddress = emailAddress;
		}
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", yearsOfEmployee='" + yearsOfEmployee + '\'' +
				", emailAddress='" + emailAddress + '\'' +
			'}';
	}
}
// end::code[]
