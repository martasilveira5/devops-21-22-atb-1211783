# Class Assignment 3

## Part I
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 3 is divided in part I and II and so there are two folders,
one for each part respectively, available at the root of this repository (ca3Part1 and ca3Part2).

In preparation for the exercises, one issue was created on Bitbucket - "#15: CA3 - Part 1 - Write technical report".

The resolution of exercises 1 to 7 followed the steps shown next.

1. Create a VM (Virtual Machine) as described in the lecture. 
![](images/Screenshot 2022-04-24 at 12.24.28.png)
  
_(Hardware characteristics of the machine used for this assignment.)_

As explained in the following article, the virtualization software to be used for this assignment
   (VirtualBox) does not support the chip Apple M1. 
https://apple.stackexchange.com/questions/422565/does-virtualbox-run-on-apple-silicon
The same happens with Vagrant, which is the software to be used in the upcoming second part of this assignment. 

To workaround this limitation, several solutions were considered:
- Using QEMU 
  - <ins>Pros</ins>: This would be a feasible solution that works well with M1, as indicated in https://gist.github.com/citruz/9896cd6fb63288ac95f81716756cb9aa.
  - <ins>Cons</ins>: It would require some extra effort to find out how to implement the required exercises. 
- Using UTM
  - <ins>Pros</ins>: As indicated by the professors, this would be a simple solution that works well with M1.
  - <ins>Cons</ins>: Vagrant does not support UTM, so the second part of the assignment would have to be done in the machine of another colleague,
as suggested by the professors. 
- Using Parallels
  - <ins>Pros</ins>: Vagrant supports Parallels, which would allow the execution of the second part of the assignment. 
  - <ins>Cons</ins>: This is a paid software. 

After considering the pros and cons, and taking into account that Parallels has a 15-day free trial, it was decided to implement this
exercise using Parallels. In the end this revealed to be a very good option, as explained in the following document.

1.1. Downloaded Parallels. 

1.2. Followed the default recommendations for installation, using the appropriate `.dmg` file. When it came to obtain the image of the 
VM, the chosen option was to download a specific UBUNTU `.iso` for the ARM64 processor. This automatically downloaded
a default `.iso` version, in this case 20.04.2.

![](images/Untitled picture.png)

![](images/Untitled picture_1.png)

![](images/Untitled picture_2.png)

![](images/Untitled picture_3.png)

1.3 The VM was automatically opened and started by Parallels after downloading UBUNTU. As it can be seen below, this UBUNTU image contains
a GUI (Graphical User Interface), on the contrary of the minimal version demonstrated by the professor. 
A password was defined, as requested by the system. 

![](images/Untitled picture_4.png)

![](images/Untitled picture_5.png)

1.4 To be able to configure the VM as requested in the lecture, the VM was shutdown first. 

![](images/Untitled picture_6.png)

1.5 Opened the configurations of the VM and chose the name _Ubuntu 20.04.2 ARM64_. The type of machine
was chosen automatically when downloading the `.iso` file and it is a Ubuntu ARM64 machine, as revealed by its name. 

![](images/Untitled picture_7.png)

1.6 The RAM was configured to 2GB under _Hardware > CPU and Memory_.

![](images/Untitled picture_8.png)

1.7 The disk was configured according the requirements under _Hardware > Hard Disk_.

![](images/Untitled picture_9.png)

![](images/Untitled picture_10.png)

1.8 Note that after some digging in the web, there doesn't seem to be configurations available for hard disk file types nor dynamic/fixed
allocation. 

![](images/Untitled picture_11.png)

1.9 Regarding network configurations, the exercise requested:
_Set Network Adapter 1 as Nat, set Network Adapter 2 as Host-only Adapter_.

However, Parallels already had two networks configured by default, "Shared" and "Host-Only", which are both DHCP, 
and therefore no further configuration was needed. These default configurations can be found under _Preferences > Network_.

![](images/Untitled picture_12.png)

_(Host-Only network IP addresses)_

![](images/Untitled picture_14.png)

_(shared network IP addresses)_

The Network adapter was configured by default too, under _Hardware > Network_ and it uses the "Shared" network.
![](images/Untitled picture_13.png)

_(Network adapter configuration)_

These default configurations revealed to be sufficient for the upcoming steps of the exercise. They allow both the access of the 
VM to the web (NAT network - named "shared") and the access from the physical machine to the VM (named "Host-Only" network), 
for example when using a remote terminal. For more details, please check point 1.12 below. 

1.10 The installation of UBUNTU was done automatically when configuring Parallels and thus it was not necessary to do so
using a CD-ROM, as demonstrated by the professor during the theoretical lesson. Linux was configured automatically by Parallels, 
so options such as language, keyboard and others were not asked upon installation. 

1.11 After having the VM working smoothly, the Linux's repository of packages was updated using its terminal:
`sudo apt update`

1.12 As indicated in the slides of the lecture, the tool for network configuration was installed...
`sudo apt install net-tools`

... and the file to configure the addresses of the Network adapter was edited using nano (Linux text editor)...
`sudo nano /etc/netplan/01-netcfg.yaml`
(note that the folder `/etc`is the place where Linux configurations are usually stored)

... the following text was added to the file...
```
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
     dhcp4: yes
    enp0s8:
     addresses:
       - 10.211.55.2/24`
```
To determine the IP address (10.211.55.2/24 - part of the Network named "Shared" shown above) and the network names (enp0s3 and enp0s8)
, the command `ifconfig` was run on the VM. This command outputs a list of nodes to which the VM is connected. 

... `CTRL + O` to save the file...

... `CTRL + X` to quit nano ...

... `sudo netplan apply` to apply the configurations setup in the file shown above. 

Even though all these steps have been followed, the access to the VM via a remote terminal, explained in the upcoming point 1.13 did not work.
Several fixes were attempted, such as creating a dedicated network adapter for the "Host-Only" network. 
However, the problem was properly fixed only when the VM was reverted to the default Network configurations, as explained in the article below and in point 1.9.
https://wpguru.co.uk/2014/02/how-to-ssh-into-a-virtual-machine-in-parallels-desktop/

1.13 The openssh-server was installed in the VM, so that ssh could be used to open a secure terminal session in the VM from another host.
`sudo apt install openssh-server`

... The configuration file for ssh was edited using nano, and the line "PasswordAuthentication yes" was uncommented, 
to enable password authentication for ssh.
`sudo nano /etc/ssh/sshd_config`  
![](images/Untitled picture_15.png)

... `CTRL + O` to save ...

... `CTRL + X` to quit nano ...

... Finally, the command `sudo service sshd restart` was run to apply the changes done. Note that this command is different 
than the recommended by the professor, which was not producing the expected results. 
This alternative was found in the article https://wpguru.co.uk/2014/02/how-to-ssh-into-a-virtual-machine-in-parallels-desktop/.
After applying it, the remote terminal login worked smoothly.

1.14 A new remote terminal was opened on the physical machine...

... `ssh parallels@10.211.55.8` 
... Note that the command uses the VM username followed by @ and the IP node
where the VM is running. The VM password was requested and the remote terminal successfuly started. 

![](images/Untitled picture_16.png)

1.15 Installed an ftp server so that the FTP protocol can be used to transfer files to/from the VM (from other hosts): `sudo apt install vsftpd ` .
Since the FTP server was enabled, it became possible to run an ftp application, like FileZilla, to transfer files to/from the VM.
Before doing so, it is also necessary to enable write access for vsftpd. For that purpose, opened nano to edit the vsftpd configuration file 
`sudo nano /etc/vsftpd.conf` and uncommented the line `write_enable=YES`
To apply the changes done ... `sudo service vsftpd restart`

1.16 Downloaded Filezilla, which is a software that allows working with a FTP (file transfer protocol) protocol.

1.17 The IP of the VM was entered in the field named "host", together with the VM's username and password in the respective fields. 
![](images/Screenshot 2022-04-24 at 19.35.25.png)

On the left side it is possible to find a list of the of the folders found in the physical local machine. On the right side
the VM's. 

In the remote terminal, the command `sudo nano readme.txt` was run to generate a sample file which was going to be transferred from 
the VM to the physical machine. The content of the file was edited and then it was dragged and dropped to the physical machine. 
The result can be found below:

![](images/Screenshot 2022-04-24 at 19.41.10.png)

1.18 Continuing in the remote terminal, git was installed in the VM... `sudo apt install git`.
The command `git --version` can be used to verify that git has been successfully installed:
![](images/Untitled picture_18.png)

1.19 JDK 11 was installed, by running the command `sudo apt install openjdk-11-jdk-headless`. Note that the version chosen
was 11 instead of 8, because this was the JDK version used to run the previous assignments, which will be tested later
in this exercise.
The command `java --version` can be used to verify that java has been successfully installed:
![](images/Untitled picture_19.png)

<ins>The steps related with the VM creation end up here.</ins>

2. Clone the individual repository inside the VM. 

2.1 First, a folder named mvsRepository was created in the VM under "Documents" using the remote terminal. 
Then my personal repository was cloned over there. 
```
cd ~/Documents
mkdir mvsRepository
cd ./mvsRepository
git clone https://martasilveira5@bitbucket.org/martasilveira5/devops-21-22-atb-1211783.git
```
2.2 Created an app password on Bitbucket to allow Linux to access it:

![](images/Untitled picture_20.png)

2.3 Below it is showed that the content of the repository has been copied to the VM under the folder mvsRepository. 

![](images/Untitled picture_21.png)

3. Build and execute the **spring boot tutorial basic project** (from the previous assignments).

3.1 **Spring boot tutorial basic project - Maven**
Using the remote terminal, changed the directory to the basic folder of ca2part2 and specifically to the folder of the alternative exercise, since this was 
the one where maven was used as a build tool. 

Maven was run:
```
cd ~/Documents/mvsRepository/devops-21-22-atb-1211783/ca2Part2/ca2Part2_alternative/tut-react-and-spring-data-rest-main/basic
./mvnw spring-boot:run 
```
Maven dependencies were installed and then Spring initiated. To test if everything is working as expected, 
it is possible to:

-Open `localhost:8080` in the browser of the VM - this is only possible, because the VM being used has a GUI.

-Open `http://10.211.55.8:8080` , i.e., < IP address of the VM >:8080 in the browser of the physical local machine. 

This was tested and the frontend of the Spring Tutorial project opened successfully:

![](images/Untitled picture_22.png)

The exercise requested that _For web projects you should access the web applications from the browser in your
host machine (i.e., the "real" machine)_. This has been demonstrated in the previous point 3.1.


3.2 **Spring boot tutorial basic project - Gradle**
Using the remote terminal, changed the directory to the basic folder of ca2part2 and specifically to the folder of the main exercise, since this was
the one where Gradle was used as a build tool.
```
cd ~/Documents/mvsRepository/devops-21-22-atb-1211783/ca2Part2/ca2Part2_mainExercise/react-and-spring-data-rest-basic
./gradlew build
```

It started by running Gradle's dependencies...
![](images/Untitled picture_30.png)

... but the build failed and the following warning was presented:
![](images/Untitled picture_31.png)

By professor's suggestion, the folder named "node" was deleted from the project, as it should have been included in the `.gitignore`
file and therefore not it should be targeted by the VCS. 
However, the error persisted. After some research, the following forum https://siouan.github.io/frontend-gradle-plugin/configuration/
suggested to alter the task named "frontend" to meet the code below. This makes the Node.js distribution used compatible 
with the workstation's architecture (ARM / Aarch64).
For that purpose, the task named `frontend` located in the file `build.gradle` was changed in the VM to:
			
![](images/Untitled picture_32.png)

After that, the build was ran in the VM's terminal and Spring initiated successfully...
![](images/Untitled picture_33.png)
![](images/Untitled picture_34.png)

To test if everything is working as expected, it is possible to:

-Open `localhost:8080` in the browser of the VM - this is only possible, because the VM being used has a GUI.

-Open `http://10.211.55.8:8080` , i.e., < IP address of the VM >:8080 in the browser of the physical local machine.

This was tested and the frontend of the Spring Tutorial project opened successfully with either of the options above:

![](images/Untitled picture_35.png)
_(using Linux's browser - firefox)_

![](images/Untitled picture_36.png)
_(using the local physical machine's browser - safari)_


5. Build and execute the **gradle_basic_demo project - Gradle** (from the previous assignments).

5.1 Using the remote terminal, changed the directory to the folder of ca2part1 and then ran `./gradle build`
![](images/Untitled picture_23.png)

5.2 Ran the command `java-cpbuild/libs/basic_demo-0.1.0.jarbasic_demo.ChatServerApp 59001` and the chat server initiated:
![](images/Untitled picture_24.png)

5.3 To visualize the chat server working, considering that the Linux version used has a GUI, there were two options:

A- To run the runClient task in the VM using the remote terminal or its own terminal.
To make it work, and as suggested in https://stackoverflow.com/questions/67391845/exception-java-lang-unsatisfiedlinkerror-when-trying-to-open-allure-reports-in-w, 
 the JDK was installed by running the command `sudo apt install openjdk-11-jdk`.
After running the task in the terminal, the chat window pops-up:
![](images/Untitled picture_27.png)
![](images/Untitled picture_28.png)
![](images/Untitled picture_29.png)

B- To run the runClient task in the terminal of the local physical machine. To accomplish that, the arguments of the task
runClient must be changed in a way that the IP of the VM is introduced, as shown below...

  ![](images/Untitled picture_25.png)


... it worked!

![](images/Untitled picture_26.png)


6. The process to resolve CA3 has been described in this document. An issue was created in BitBucket for that 
purpose (#15: CA3 - Part 1 - Write technical report) and several commits took place, such as the one below:
```
cd /Users/martasilveira/Documents/"Switch 2021"/DevOps/DevOpsMVS/devops-21-22-atb-1211783
git add . 
git commit -a -m "Addresses #15 - create readMe.txt file to start documentation."
git push origin main
```

7. After the completion of this technical report, the repository was marked with the tag ca3-part1.
```
git tag ca3-part1
git push origin ca3-part1
```