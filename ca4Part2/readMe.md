# Class Assignment 4

## Part II
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 4 is divided in part I and II and so there are two folders,
one for each part respectively, available at the root of this repository (ca4Part1 and ca4Part2).

In preparation for the exercises, two issues were created in Bitbucket - "#20: CA4 - Part 2 - Implement proposed exercises"
and "#21: CA4 - Part 2 - Write technical report".

**Lecture Exercises**

Created a new folder named "lectureExample". Placed the `cd` on that folder and ran `git clone https://bitbucket.org/atb/docker-compose-spring-tut-demo.git`
to clone the project demonstrated in the lecture by the professor.
This project is an example of a `docker compose` that sets up 2 containers, similarly to what has been done in CA3 part2:
- A container that will run Tomcat and the Spring tutorial application using JAVA and REACT.
- A container that will run H2 and access the DB. 

There is a dockerfile for each of these containers and there is also a docker compose file, which contains:
- An indication of the docker files to be used for each service/container (`build: <name of the service>`).
- `ports` -> in our docker compose file this command indicates that the port 8080 from the container will appear in the 8080 port of the host machine.
- The command `depends on` forces that the container `db` is launched before the `web` one. 
- We also configure the `volumes` that will be used for copying files between the database and the host machine.

To put all this into practice, the docker desktop application was initialized.
The `cd` was placed on the folder `docker-compose-spring-tut-demo` and then the command `docker-compose build` was run
to create a new docker image based on the docker compose file mentioned previously.
Several errors popped-up related with the fact that the host machine being used runs an Apple Silicon M1 Chip (ARM64).
Some of those errors were:

![](images/Screenshot 2022-05-19 at 10.19.17.png)
(could not install node due to the architecture arm64)

![](images/Screenshot 2022-05-19 at 10.19.32.png)
(could not install npm)

To fix this, the Dockerfile that generates the web service (aka container) had to be adapted as described below:
- As mentioned in CA3 part 2, the implementation of the `tut-basic-gradle` project required the following software versions to run on
an M1 (ARM64) machine:
  - A box compatible with the architecture ARM64, which would have to be Ubuntu 20.04 ARM64, which in its turn demanded:
    - TomCat version 9.
    - JDK 11.
    - Node.js 16.0.0 or higher.
    - Gradle 6.5.0 or higher.
    - The plugin org.siouan.frontend-jdk11 version 6.0.0 or higher.
- In accordance to these requirements, a different docker image had to be used, specifically one that had tomcat installed by default.
- However, because that image did not have jdk11 installed by default, some lines were added to the docker file for this purpose.
- The same was done for node.js, since it was required to have a version equal or higher than 16.0.0. 
The script used (below) installs npm too as a dependency of nodejs (source: https://computingforgeeks.com/how-to-install-node-js-on-ubuntu-debian/):
```
RUN curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
RUN sudo apt install nodejs -y
```
- Instead of professor's repository, I used my own, because it already had the needed gradle configurations to run on an 
ARM64 architecture (node and npm versions as well as siouan frontend plugin for jdk11). So my own BitBucket repository
was cloned to the container and the `cd` was placed on the folder of Ca3 Part2.

The command `docker-compose build` was run again and a new image was created. 

Afterwards, the containers were started using `docker-compose up`.
This command:
- _Builds images from Dockerfiles_
- _Pulls images from registries_
- _Creates and starts containers_
- Configures a bridge network for the containers, allowing their connection to the internet and to the host machine. 

At this point a problem rose. Even though "catalina.sh" (the script that is actually responsible for starting Tomcat) started, 
the webpage would not be displayed. 

To try to understand if gradle was running correctly and producing the expected outputs as a result of a successful build, 
we tried to run `./gradlew build` directly in the terminal of the web container. This confirmed that the `.war` files were being
generated correctly and in the correct directory. 
To make this check, the following commands were run:

`docker compose exec web bash` -> to open a new terminal in the container.

Multiple `cd ...` and `ls -l` followed to get to the folder `build/libs/` :

![](images/Screenshot 2022-05-19 at 16.10.18.png)

At this point we concluded that probably there was a problem with the network configuration. So we removed it from the `docker-compose.yml` file
and left only the necessary configuration of the ports. 

Then we realized that Spring did not initialize and that probably this was due to the fact that the TomCat version installed was too recent. 
So, we specified in the dockerfile the Tomcat version compatible with jdk11 to be installed: `FROM tomcat:9.0-jdk11`. 

Finally, we had to change the `application.properties` file under the folder `resources` belonging to the 
`tut-basic-gradle project. 
Considering that we had deleted the network cnfigurations, we had to change the `spring.datasource.url` to `jdbc:h2:tcp://db:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
`Basically, we replaced the IP of the container `db` by its own name, considering that we had not configured any specific IP address for it. 

After that, the application started sucessfully:

![](images/Screenshot 2022-05-19 at 15.59.48.png)

![](images/Screenshot 2022-05-19 at 15.59.58.png)

When opening `http://localhost:8080/basic-0.0.1-SNAPSHOT/`:
![](images/Screenshot 2022-05-19 at 16.00.14.png)

When opening `http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console` and entering the URL `jdbc:h2:tcp://192.168.33.11:9092/./jpadb`
as recommended in the readMe.md file from project `docker-compose-spring-tut-demo`:
![](images/Screenshot 2022-05-19 at 16.31.50.png)

If we run `docker-compose ps` we see both containers running:

![](images/Screenshot 2022-05-19 at 16.12.04.png)

If we open a terminal in the container `docker compose exec web bash` and try to check the processes running with `ps -ax`:
![](images/Screenshot 2022-05-19 at 16.12.54.png)


At this point, I realized that exercises 1 and 2 of CA4 part2 had already been completed in the steps above:

_The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle 
version of the spring basic tutorial application._

**Exercises resolution**
3. To publish the just created images to Docker Hub, the following steps were followed:
```
docker tag e90d0aaa1398 mvs5/new_repo:web
docker tag 56d55289aef9 mvs5/new_repo:db
```
By running `docker images` it is possible to check that the tag has been successfully added:
![](images/Screenshot 2022-05-19 at 19.40.35.png)

And then, as recommended by docker hub, we used the commands below to push the images to the repositories.
```
docker push mvs5/new_repo:web
docker push mvs5/new_repo:db
```

The images have been successfully published as shown below:
![](images/Screenshot 2022-05-19 at 19.54.03.png)


4. To resolve this exercise, we will log into the `db` container bash shell by running `docker compose exec db bash ` while on the folder
that has the docker compose file. 
After that, by running `ls -l` we see the `.db` file that we need to copy into the volume configured in the docker compose file:

![](images/Screenshot 2022-05-19 at 16.59.28.png)

To copy the `.db` file to our host machine, we will use the volume configured and the command `cp ./jpadb.mv.db /usr/src/data-backup`
The destination path indicated, is the same as defined in the docker compose file. 
After a couple of seconds, we see the new file popping up in the `data` folder, as expected! 

![](images/Screenshot 2022-05-19 at 17.02.58.png)

5. All the steps followed have been duly described in this document. 

6. After terminating this document, all issues were resolved and my own BitBucket repository was tagged as follows:
```
git tag ca4-part2
git push origin ca4-part2
```

###Alternative Analysis
####Option 1 - Kubernetes

_Docker is about building individual containers while Kubernetes is about managing and orchestrating large numbers of them._

**What is Docker?** 

Docker is an open-source technology for automating the deployment of applications as portable,
self-sufficient containers. Docker builds containers that are configured to have all the necessary components required for them to function in isolation, 
including code, dependencies and libraries. It also allows sharing container images through a
container registry like Docker Hub. Docker has
grown to be the default container format in recent years. 

**What are Docker's advantages?**

- Portability — you can run these containers wherever you want.
- Security — containers are quite well isolated. 
- Efficiency - they increase efficiency for CI/CD processes. 
- Reusable and sharable.

**And what are its disadvantages?**

- Persistence - as containers are meant to be ephemeral, persistent data storage can be an issue. 
- They inherently introduce additional layers, which adds additional overhead compared to applications running directly on a bare metal platform. 

**What is Kubernetes aka K8s?** _K8s is like the ultimate game of Tetris — it chooses the placement of containers to optimize computing resources (...)._ 
This is called orchestration. 

Kubernetes is an open-source orchestration software that provides an API to control how and where those containers will run.
It helps you to tackle some of the operating complexities when
using multiple containers, deployed across multiple servers.
All Kubernetes deployments have at least one cluster. A cluster contains nodes. These nodes host pods. These pods include a running set of containers. 
A Kubernetes instance has a control plane to manage all these components, as shown below. 

![](images/Screenshot 2022-05-19 at 23.15.30.png)

**What are K8s' advantages?**

- It addresses the need for enterprises to commonly orchestrate container deployment. 
- Having numerous users around the globe, it benefits from a big community of users that may share insights and help clear problems out.
- Kubernetes comes with a powerful API and command line tool, called kubectl, which handles a bulk of the heavy lifting
  that goes into container management by allowing you to automate your operations.
- Infrastructure abstraction - Kubernetes manages the resources made available to it on your behalf. This frees developers to focus on writing
  application code and not the underlying compute, networking, or storage infrastructure.
- Service health monitoring - It performs automated
  health checks on services and restarts containers that have failed or stopped. Kubernetes only makes services
  available when they are running and ready.

**And disadvantages?**

- K8s was built with massive enterprise-scale use in mind. K8s thus may be an overkill for smaller projects. 
- Kubernetes also requires significant upfront training, and, once running, it can be a lot to maintain and update over time.

**Docker vs. Kubernetes: What’s The Difference?**

_The difference between the two is that Docker is about packaging containerized applications on a single node and 
Kubernetes is meant to run them across a cluster._

On one hand smaller-scale enterprises may benefit from the usage of Docker rather than Kubernetes, as the latter
are made for large scale projects and require some overhead for maintenance. On the other hand, larger enterprises will definitely benefit
from the orchestration power of Kubernetes. 

However, this is not an "either or" question. _The difference between Kubernetes and Docker is more easily understood 
when framed as a “both-and” question._
Kubernetes and Docker are fundamentally different technologies that work well together for building, delivering, and scaling containerized apps.
In short, use Kubernetes with Docker to:
Make your application more scalable. If your app starts to get a lot more load and you need to scale out to be able to
provide a better user experience, it’s simple to spin up more Docker containers or add more nodes to your Kubernetes cluster.

Kubernetes lets you orchestrate a cluster of virtual machines and schedule containers (such as Docker containers) to run on those virtual machines 
based on their available compute resources and the resource requirements of each container.

**How could Kubernetes be used to solve the same goals as presented for this assignment?**
As mentioned previously several times, Kubernetes does not act as a replacement of Docker. Hence, I have made some search about how 
it would be possible to integrate the Docker containers into a Kubernetes solution as part of this exercise. 

1. All the steps followed as part of this exercise would be mostly kept unchanged: the creation of the spring-react application, 
the configuration of the docker files and the generation of the docker image using the `docker build`
command.
2. After that it would be necessary to deploy the Docker image. For that purpose, in the root directory, a new `.yaml` would be created. 
This file would deploy the application to the Kubernetes engine and it must have two parts:
- Service - The service acts as the load balancer. A load balancer is used to distribute requests to the various available servers.
- Deployment - this will act as the intended application. 

The user request hits the load balancer, then the load balancer distributes the 
request by creating the number of replicas defined in the `deployment.yaml` file. For example, if we have five 
replicas for scalability, it means that we will have 5 instances running at a time. The benefit of multiple replicas is 
that if an instance crashes, the other application instances continue running.

By building a file `deployment.yaml` with such characteristics, the docker image will automatically create containers for the application when we deploy it.

3. The final step is deploying to the Kubernetes service. Execute the command below in your terminal:
```
kubectl apply -f deployment.yaml
```

This command will deploy our service and application instances to the Kubernetes engine.
...aaaand it's done! There are several other actions that can be taken after this step such as viewing a deployment dashboard.

**Sources of information:**
https://containerjournal.com/editorial-calendar/best-of-2021/whats-the-difference-between-docker-and-kubernetes
https://azure.microsoft.com/en-us/topic/kubernetes-vs-docker/
https://www.atlassian.com/microservices/microservices-architecture/kubernetes-vs-docker
https://www.section.io/engineering-education/deploy-docker-container-to-kubernetes-cluster/





