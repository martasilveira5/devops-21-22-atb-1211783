# Class Assignment 4

## Part I
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 4 is divided in part I and II and so there are two folders,
one for each part respectively, available at the root of this repository (ca4Part1 and ca4Part2).

In preparation for the exercises, two issues were created in Bitbucket - "#19: CA4 - Part 1 - Implement proposed exercises"
and "#18: CA4 - Part 1 - Write technical report".

**Lecture tasks**
In the first part of this document, we will describe the steps followed during the DevOps lecture and how the proposed 
tasks were implemented. 

1. Installed Docker Desktop, by downloading the `.dmg` file from the web: https://www.docker.com/products/docker-desktop/.
Accepted the terms and conditions required by Docker. 

2. It is possible to verify that docker has been correctly installed by running `docker --version`:

![](images/Screenshot 2022-05-12 at 10.01.57.png)

Then, created the folder `lectureExercises`.

3. Ran: `docker run --name my-ubuntu-container -i -t ubuntu /bin/bash`
This command creates a container with the name "my-ubuntu-container" from the image "ubuntu". The commands `-i -t` allow
the creation of an interaction session in the terminal. 

![](images/Screenshot 2022-05-12 at 10.11.49.png)

Since the image was not yet available locally, docker grabbed the latest version automatically from the web as part of the
step described above. This was confirmed when checking the section "Images" in Docker Desktop:
![](images/Screenshot 2022-05-12 at 10.12.42.png)

Besides, as it can be seen above, Ubuntu's bash command line was prompted automatically - `root@95e22ee26ba5:/# `.

When running the command `ps`, it can be seen that the bash command line and the `ps` job itself, are the only ones running
in the Docker container:

![](images/Screenshot 2022-05-12 at 10.16.04.png)

4. In another terminal, it is possible to confirm that the container is running by typing the command `docker ps`. 
It is also possible to verify this by opening the Docker Desktop App. Both options are shown below. 

![](images/Screenshot 2022-05-12 at 10.17.57.png)

![](images/Screenshot 2022-05-12 at 10.19.00.png)

5. The command `docker stop ...container name or ID...` will stop the container. This can be verified by running `docker ps`
again or by consulting the Desktop Application, as shown below:

![](images/Screenshot 2022-05-12 at 10.22.09.png)

![](images/Screenshot 2022-05-12 at 10.22.19.png)

6. The command `docker ps -a` will show all containers, including the ones that have been exited, as shown below:

![](images/Screenshot 2022-05-12 at 10.24.20.png)

To restart the container, the command `docker start my-ubuntu-container` must be run, since the container had already 
been initiated with `docker run`.

7. When running `docker images` it is possible to confirm that the only image available at the moment was the one 
downloaded automatically in point (3):

![](images/Screenshot 2022-05-12 at 10.40.06.png)

8. Let's try now to build our own image. For that purpose, a text file named `dockerFile` was created and the content 
of the lecture's slides was copied to that text file.

The command `FROM` is used to state that we want to create an image from an already existing image. 
This website https://hub.docker.com contains all available docker images and there it is also possible to verify the ones
that have been created with our own account. We can also verify that, happily, the ubuntu image chosen in point 3 is compatible with the processor ARM64.
This is fortunate, considering that the host machine being used is a MacBook Pro with Apple Silicon M1. 

The command `RUN` allows executing commands while building an image in a layer on top of it. There can be several RUN
commands in the docker file. 

The command `ENV` is used to create environment variables, as required by Apache. 

The command `EXPOSE` basically says which port is going to be used for this container. In this case `EXPOSE 80` will define
the port `localhost:8080`.

The command `COPY` allows the copy of files from the host machine to the container. 

The command `CMD` - its purpose is to provide defaults for an executing container. There can be only one CMD instruction inside a Dockerfile.

(source _https://phoenixnap.com/kb/create-docker-images-with-dockerfile_)

As recommended in the link above, the DockerFile was created from scratch and added to `ca4Part1/dockerImage` folder. 
Afterwards, the command `docker build -t my_apache_image .` was run. This names the image as "my_apache_image" and assumes that
the dockerFile which will produce the new image is located at the current directory:
```
cd /Users/martasilveira/Documents/"Switch 2021"/DevOps/DevOpsMVS/devops-21-22-atb-1211783/ca4Part1/dockerImage
docker build -t my_apache_image .
```

The first error prompted:
```failed to solve with frontend dockerfile.v0: 
failed to read dockerfile: open /var/lib/docker/tmp/buildkit-mount936475294/Dockerfile: 
no such file or directory
```

After some research (source: https://stackoverflow.com/questions/64985913/failed-to-solve-with-frontend-dockerfile) 
it was understood that naming the docker file with camelcase was not a good idea. The error was fixed and `docker build -t my_apache_image .`
command was run again. 

This time, docker complained that no file named "public-html" could be found. 
A new html file was created and placed on the same folder as the docker file. 
The command `docker build -t my_apache_image .` was run again. 

After that, by running `docker images` we see that a new image has been created:

![](images/Screenshot 2022-05-12 at 14.11.23.png)

Then we ran `docker run -p 8081:80 -d my_image` to start the container with the image just created. We chose 8081 instead
of 8080, to avoid a conflict with the other container created previously.
After running `docker ps`, we see that this container has been successfully created:

![](images/Screenshot 2022-05-12 at 15.05.38.png)

Now let's try the browser and verify if we can see html page `public-html`:
![](images/Screenshot 2022-05-12 at 15.08.52.png)

The empty html page with the title "Title" was successfully loaded as it can be seen above. 


**Implementation of CA4 part 1 exercises**

1. Created an account on Docker Hub: https://hub.docker.com/choose-plan?ref=signup
2. Created new folders named "exercises1" and "exercises2" to store the files used for the resolution of this exercise
   (version 1 and version 2 respectively).
3. Version _(1)In this version you should build the chat server "inside" the Dockerfile_ .
For this one, we created the Dockerfile as it can be found in this repository: `ca4Part1/exercises1/Dockerfile`
Note that:
- The main structure of the file is copied from the file used in the lecture exercises.
- Two new lines were added, to install git and java jdk8 (lines 8 and 9).
- The command `WORKDIR` was used to create the folder where the project gradle_basic_demo will be copied to and to set the `cd`
in that same folder. 
- The command `RUN` was used to clone the professor's repository and to run the gradle build commands. 
- The port for the chat server was defined using the command `EXPOSE`.
- Finally, the command CMD was used to run the chat server.

After having the docker file ready, the command `docker build -t gradle_basic_demo .` was run and the image 
was created successfully as shown below...

![](images/Screenshot 2022-05-12 at 17.02.48.png)

...which is proved by the output of the command `docker images`:

![](images/Screenshot 2022-05-12 at 17.05.25.png)

Then the container was started `docker run -p 59001:59001 -d gradle_basic_demo`...

![](images/Screenshot 2022-05-12 at 17.16.09.png)

To see the chat working, we moved the `cd` to the folder where the gradle_basic_demo project is located in our local machine and ran the command 
`./gradlew runClient`
The chat works!! :)

![](images/Screenshot 2022-05-12 at 17.20.15.png)

![](images/Screenshot 2022-05-12 at 17.18.28.png)

After confirming the success, the container was stopped to ensure that the needed port is free:
`docker stop 3f9cbaa43325`

4. Version _(2)In this version you should build the chat server in your host computer and copy the jar
   file "into" the Dockerfile1_
For this purpose, we created the Dockerfile as it can be found in this repository: `ca4Part1/exercises2/Dockerfile`
Note that:
- The main structure of the file is copied from the file used in the lecture exercises.
- A different image was used as a base for this customized image (openjdk:11). In fact, this avoids the need to configure 
the installation of java jdk in the docker file, because this image brings the jdk installed by default. 
- Similarly to version 1, a line was added to install git.
- The `WORKDIR` command was used again, this time to set up the folder to where the jar file is going to be copied.
- The `COPY` command was used to copy the `.jar` file generated by gradle when running `./gradlew build` on the host machine. 
This file will be copied to the new container, because it is necessary to run `./gradlew runServer` and to start
the chat server on the container. 
- Again we use the command `EXPOSE` to define the port for the chat server.
- Finally and similarly to what was done previously, the command `CMD` was used to run the chat server.

After having the docker file ready, we moved to a terminal in my host machine and placed the current directory on 
professor's project gradle_basic_demo. Gradle was run to generate the needed `.jar` file.
```
cd /Users/martasilveira/Documents/"Switch 2021"/DevOps/DevOpsMVS/devops-21-22-atb-1211783/ca2Part1/luisnogueira-gradle_basic_demo-d8cc2d7443c5`
./gradlew clean build
```

The build was done successfully and the `.jar ` file was created:
![](images/Screenshot 2022-05-12 at 17.51.27.png)

This `.jar` file was copied over to the CA4 folder: `/Users/martasilveira/Documents/Switch 2021/DevOps/DevOpsMVS/devops-21-22-atb-1211783/ca4Part1/exercises2`
This is the folder that we defined as a source in the docker file from where to copy the jar file. 

Then, we moved to docker and created a new image based on the docker file just created:
```
cd /Users/martasilveira/Documents/"Switch 2021"/DevOps/DevOpsMVS/devops-21-22-atb-1211783/ca4Part1/exercises2
docker build -t gradle_basic_demo2 .
```

The image was successfully created:
![](images/Screenshot 2022-05-12 at 18.17.35.png)

Then the container was started using the just created image and the defined port 59001:
`docker run -p 59001:59001 -d gradle_basic_demo2`

It was confirmed that the new container is running:
![](images/Screenshot 2022-05-12 at 18.37.00.png)

And finally, to visualize the chat, the task "`./gradlew runClient`" was run on our local machine. IT WORKS!!! :)
![](images/Screenshot 2022-05-12 at 18.39.46.png)

![](images/Screenshot 2022-05-12 at 18.41.55.png)

5. As requested in the exercise requirements, the images that were just created were tagged. 
Their original tag was named "latest" and it was replaced by their own image names.
The following commands were used:
```
docker images
docker tag gradle_basic_demo2:latest gradle_basic_demo2:gradle_basic_demo2
docker tag gradle_basic_demo:latest gradle_basic_demo:gradle_basic_demo
```

This can be confirmed by running `docker images` once more:

![](images/Screenshot 2022-05-12 at 20.20.48.png)
(_before the tag was done_)

![](images/Screenshot 2022-05-12 at 20.20.56.png)
(_after the tag was done_)

5. To publish both created images to docker hub, as recommended in https://docs.docker.com/docker-hub/repos/, 
first we created a repository:
![](images/Screenshot 2022-05-12 at 20.32.01.png)

And then, as recommended by docker hub, we used the commands below to push the images to the repositories.
```
docker push mvs5/new_repo:gradle_basic_demo2
docker push mvs5/new_repo:gradle_basic_demo
```

After doing this, we realized that we had tagged the images incorrectly. The tags must contain the username, followed by a 
slash and the name of the repository.
This was fixed, using the images' IDs and the command below:
```
docker tag 2374b024ea5f mvs5/new_repo:gradle_basic_demo2
docker tag 116a3918eb8f mvs5/new_repo:gradle_basic_demo 
```

Push was tried again:
```
docker push mvs5/new_repo:gradle_basic_demo2
docker push mvs5/new_repo:gradle_basic_demo
```

This time, login was requested: `denied: requested access to the resource is denied`
So we ran `docker login` and then typed the username and password, as shown below.
![](images/Screenshot 2022-05-12 at 20.42.08.png)

After logging in, we ran the docker push once more:
```
docker push mvs5/new_repo:gradle_basic_demo2
docker push mvs5/new_repo:gradle_basic_demo
```

Finally, it was confirmed on the browser and on Docker Desktop that the images were published on docker hub successfully:
![](images/Screenshot 2022-05-12 at 20.47.07.png)

![](images/Screenshot 2022-05-12 at 20.48.37.png)

6. After terminating this document, all issues were resolved and my own BitBucket repository was tagged as follows:
```
git tag ca4-part1
git push origin ca4-part1
```


**Final Notes**

The first version of the docker file basically packs up the whole gradle_basic_demo, whereas the second version of the 
docker file copies just the `.jar` file. This allows the creation of a container to run the chat server in a much lighter way.
Then my local host machine is used as a client to visualize the chat window, because the container created does not have a GUI. 


