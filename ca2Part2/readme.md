# Class Assignment 2

## Part II
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 2 is divided in part I and II and so there are two folders, 
one for each part respectively, available at the root of this repository (ca2Part1 and ca2Part2). For part II, both the main
exercise and its alternative were implemented. There is a dedicated folder for each of them: ca2Part2_mainExercise and 
ca2Part2_alternative, respectively. Besides, the resolution of this second part is based on the repository react-and-spring-data-rest 
and it is focused on the basic folder. Please note that for the main exercise, the basic folder can be found under ca2Part2_mainExercise.
For the alternative, the whole react-and-spring project can be found under ca2Part2_alternative and under that, there is the basic folder which is
going to be used for the alternative implementation. 


In preparation for the exercises, 3 issues were created on Bitbucket.
The resolution of exercises 1 to 17 followed the steps shown next. 

1. 
* Created folders ca2Part2_mainExercise and ca2Part2_alternative.
* Downloaded the repository "spring tutorial" from Github to the folder ca2Part2_alternative. 
* Created an intial version of this readme.md file. 
* Ran the following VCS commands to commit and push the latest changes and then to create a new branch
named "tut-basic-gradle":
```
git add . 
git commit -a -m "Addresses #12, add Spring Tutorial files to ca2Part2 and readme.md file"
git push origin main

git branch tut-basic-gradle
gitcheckout tut-basic-gradle
```
2. On the browser, navigated to https://start.spring.io and filled the form as indicated by the professor in the theoretical
class:
![](images/Screenshot 2022-04-07 at 22.03.43.png)
All four dependencies were selected (rest repositories, thymeleaf, spring data JPA and H2 database), as well as the gradle option, 
JDK11 and jar packaging. 

3. Generated a zip file based on the configurations mentioned in point 2 and downloaded it directly to the folder ca2Part2_mainExercise. 
At this point, please note some important points such as:
* After unzipping the file, a new folder named react-and-spring-data-rest-basic was created. This folder contains a project with
an organized Gradle structure where it can be found a default JAVA class named "DemoApplicationTests". This class will be replaced 
later by the src files from the tut-react-and-spring-data-rest project, as explained in points 4 and 5. 
* The **build.gradle** file was created automatically and its dependencies contain the ones chosen in the previous point:
![](images/Screenshot 2022-04-07 at 22.30.08.png)
* The name of the project was stored in the file settings.gradle. 
* A folder named **gradle.wrapper** was created automatically. The files in this folder guarantee that, whenever someone  
runs this project, the correct Gradle version is utilized, even if the user doesn't have Gradle installed
locally in its machine and without having to deliberately download it. 

Afterwards, the current directory was moved to...
`cd /Users/martasilveira/Documents/"Switch 2021"/DevOps/DevOpsMVS/devops-21-22-atb-1211783/ca2Part2/ca2Part2_mainExercise
/react-and-spring-data-rest-basic`

... and the available tasks were checked using the command `./gradlew tasks`, as shown below:
![](images/Screenshot 2022-04-07 at 22.39.06.png)


4. As mentioned before, the "DemoApplicationTests" JAVA code will be discarded and for that purpose the folder src was deleted. 

5. The folder "src" was copied from the project tut-react-and-spring-data-rest-main and pasted on react-and-spring-data-rest-basic 
(the folder mentioned in point 3) - this operation was done using Finder (on MacOS). The same was done for the files webpack.config.js and
package.json. By copying the source code of the project that we want to transform into a Gradle project to react-and-spring-data-rest-basic, 
we are ensuring that this source code is now using the configurations of a Gradle project. 
> This exercise had a final request asking to delete the folder src/main/resources/static/built. However, the project had not been built
> yet and therefore this folder didn't exist either.

6. The application was tested by running `./gradlew bootRun`
Spring was initiated correctly:
![](images/Untitled picture.png)
When opening `localhost:8080` on browser, nothing is displayed.
![](images/Untitled picture_1.png)
This was expected, because we copied the JAVA code and its files, but Gradle is not yet processing the javascript files (to be done next). 
It is missing a plugin to deal with the frontend files (in Maven this plugin is found on POM.XML).

At this point, the changes were committed to the remote repository:
```
git add .
git commit -a -m "Addresses #12, complete exercises 1 to 6 of ca2_part2"  
git push origin tut-basic-gradle
```

7. The plugin "org.siouan.frontend" was used to replace "frontend-maven-plugin", as described in the next point.
8. Since this project uses JAVA JDK11, as chosen in the configurations described in point 2, the line 
'id "org.siouan.frontend-jdk11" version "6.0.0"' was added to the plugins block in the file **build.gradle**, as shown below.
![](images/Screenshot 2022-04-08 at 12.13.03.png) 

9. The following frontend block was added to the file **build.gradle** too:
![](images/Untitled picture_2.png)

10. The file **package.json** contains configurations related to the application frontend. Having said that, the code "scripts" 
below was copied to that same file. This will configure the execution of the webpack, and replaces the pre-existing "scripts" code:
![](images/Screenshot 2022-04-08 at 12.20.54.png)
This webpack tool will get all the javascript files from the application and it will create the build folder that was removed in point 5, which
generates the bundle.js file. 

11. After running the command `./gradlew build` again:
![](images/Untitled picture_3.png)
The build was successful and the folder src/main/resources/static/built is shown again in the project structure:
![](images/Screenshot 2022-04-08 at 13.52.37.png)
The tasks related to the frontend were executed too and the frontend code was generated. This means that we can now run Spring 
and see the frontend, as shown in step 12.

12. After executing the command `./gradlew bootRun `...
![](images/Untitled picture_4.png)
... and navigating to the browser `localhost:8080`, the frontend is available again:
![](images/Untitled picture_5.png)

13. For this exercise, the jar files located at build/libs are to be copied to a new folder named "dist".
![](images/Untitled picture_6.png)
It was created a task named copyJar, of type "Copy", as shown on the right below. It was placed on the file **build.gradle**. 
This task contains only two attributes: the source of the files to be copied "from" and the destination of those files "into".
After running the task, the result obtained is shown on the left - the folder dist appears at the root level of the project
and it contains the two jar files.
![](images/Screenshot 2022-04-08 at 14.24.32.png) 

![](images/Untitled picture_7.png)

14. The objective of this exercise is to add a task to gradle to delete all the files generated by webpack 
(src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.
This means that the task clean will become dependent on another task that actually deletes the files
located in the path mentioned before. 
With that purpose in mind, the task "deleteWebpack" was added to the file **build.gradle**, as shown below. 
Besides, a dependency was included in the "clean" task with that other "deleteWebpack".

![](images/Untitled picture_8.png)

15. To better understand how it is possible to test the changes implemented in point 14, it is important to retain that
the gradle task "clean" is an "isolated" task which is part of JAVA plugin. This task is not dependent nor evoked by any other task, therefore it has
to be called directly, so that the effects of the "deleteWebpack" task can be observed. 
(check the flowchart below - the task "clean" is found at the bottom left corner, isolated from the remaining. This chart 
was taken from DevOps support material, the author is Professor Alexandre Bragança)
![](images/Untitled picture_9.png)

Having said this, the command `./gradlew clean` shall delete all the JAVA files (this is the default action) and also the
folder src/resources/main/static/built/. This was verified as shown below:

![](images/Untitled picture_10.png)
	
After confirming that all the implemented features were working as expected, the work was committed, pushed and the branch
tut-basic-gradle was merged with main:

```
git add . 
git commit -a -m  "Addresses #12, resolve exercises 7 to 15."
git push origin tut-basic-gradle
git checkout main
git merge tut-basic-gradle
git push origin main
```

16. The steps followed to resolve the proposed exercises were documented above. Several commits can be found in the remote repository, 
under issue #13, to submit this document. In the next section, it is presented an explanation of how the alternative
exercise was implemented, together with an analysis of both options. 

17. After point 16 was finished, the following commands were executed to tag the last commit: 

```
git tag ca2-part2
git push origin ca2-part2
```

____________________________
## Part II - Alternative Exercise using Maven
###Alternative Implementation

Maven was chosen as the alternative used to implement the exercises of this class assignment number 2. 
A dedicated folder was created for this alternative exercise - ca2Part2_alternative - and the whole tut-react-and-spring-data-rest
project was copied over there. Since this project's structure and configuration are already based on Maven, 
exercises 1 to 12 are already solved. No action is needed. 
It is possible by default to run Spring and visualize the frontend by running...
`./mvnw spring-boot:run`
![](images/Untitled picture_11.png)
... and then opening `localhost:8080` on browser...
![](images/Untitled picture_12.png)

This proves that exercises 1 to 12 are completed without any action needed. 

Before heading to exercise 13, a new branch was created in the remote repository, as demanded by the exercise:
```
git branch tut-basic-maven
git checkout tut-basic-maven
```

13. To complete exercise 13, it was necessary to understand that the .jar files to be copied over to a folder named "dist" 
had not yet been generated and that they could not yet be found in the folder named "target" (where they are supposed to be stored
automatically by Maven). To work that around, the current directory was moved to the basic folder and the command 
`./mvnw clean install` was executed (this command is a sort of equivalent to `./gradlew build`, an explanation will be provided later in 
this document). After that, the .jar files showed up under `target` as shown below:
![](images/Untitled picture_13.png)

The approach followed to copy these .jar files into a folder named "dist", located at the root of the repository, 
was to use a plugin. This plugin was inserted into the `<build>` section of the file `pom.xml` located at
`ca2Part2/ca2Part2_alternative/tut-react-and-spring-data-rest-main/basic`, as recommended in: 
https://maven.apache.org/plugins/maven-resources-plugin/examples/copy-resources.html

![](images/Screenshot 2022-04-08 at 22.09.29.png)

As it can be seen above:
* This plugin is named maven-resources-plugin - defined by the tag `<artifactID>`
* It was implemented at the build **phase** named "install" (more details will follow) - defined by the tag `<phase>`
* The directory where the files will be pasted is defined by the tag `<outputDirectory>`. In this case, it was defined that
the files will be pasted on a folder named "dist" located at the root of the repository. 
* The directory where the files to be copied are found is defined by the tag `<directory>` and the files to be copied are
defined by the tags `<includes>` and `<include>`.

To test this plugin, it was necessary to run a command equivalent to `./gradlew build`.
As indicated in https://www.journaldev.com/8396/gradle-vs-maven, the command ran was `./mvnw install` and it was verified 
that the folder `dist` was created at the root of the project "basic" and that the jar files were effectively copied over there. 
	
![](images/Screenshot 2022-04-10 at 11.33.02.png)
	
To better understand why the phase chosen for this plugin was "install", let's take a look at the table below:
(this table is taken from the support material of DevOps theoretical classes, author: Professor Alexandre Bragança)
![](images/Untitled picture_14.png)

As it can be seen, the install phase is the latest one happening in the local machine. This was the phase chosen, so that the copy
of the files would happen after they were created (during the package phase). In practice, this means that whenever the commands `./mvnw install`
or `./mvnw clean install` are executed, the copy will also be done. However, if commands like `./mvnw test package` are ran instead, 
the copy won't be done, because the install phase is not reached. As explained in the "Alternative Analysis" of this document:
_Maven is based on a linear model of phases_.

At this point, a commit was done to the remote repository, as a backup:
```	
git add . 
git commit -a -m "Addresses #14, resolves exercise 13 for the alternative version"
git push origin tut-basic-maven
```

14. Before digging deeper into this exercise, it is important to understand under which context the task `./mvnw clean` is executed. 
As shown in the flowchart below (source: https://medium.com/@yetanothersoftwareengineer/maven-lifecycle-phases-plugins-and-goals-25d8e33fa22),
and similarly to what happens in Gradle, the clean task is independent of the Maven build lifecycle.
![](images/Screenshot 2022-04-10 at 12.01.53.png)

As mentioned in https://maven.apache.org/plugins/maven-clean-plugin/usage.html:
>The Maven Clean Plugin, as the name implies, attempts to clean the files and directories generated by Maven during its build. 
> While there are plugins that generate additional files, the Clean Plugin assumes that these files are generated inside the target directory.

This assumption applies to the project being used for this exercise, tut-react-and-spring-data-rest-main/basic, which has a default maven configuration. 

Going back to the exercise, besides the files located at the "target" folder, it requests the deletion of the files generated by Webpack, 
which are located at src/resources/main/static/built.
Based on the article https://maven.apache.org/plugins/maven-clean-plugin/examples/delete_additional_files.html there is a plugin 
named "maven-clean-plugin" that deletes additional non-default files when running the task "clean".

This plugin was inserted into the `<build>` section of the file `pom.xml` located at
`ca2Part2/ca2Part2_alternative/tut-react-and-spring-data-rest-main/basic`:

![](images/Untitled picture_15.png)

The tag `<directory>` defines the relative path of the location of the files to be deleted. 
The tags `<includes>` and `<include>` define the files to be deleted. 

15. To test the previous exercise, the command `./mvnw clean` was run and it was verified that the content of the folder 
"built" was actually deleted:
![](images/Screenshot 2022-04-10 at 12.32.33.png)

To finalize this alternative implementation, the following git commands were executed to commit the work done, push it to the 
remote repository and to merge the branch used for these changes with the main branch. 
```	
git add . 
git commit -a -m "Addresses #14, resolve exercises 14 and 15"
git push origin tut-basic-maven
	
git checkout main
git merge tut-basic-maven
git push origin main
```

16. All the steps followed to implement the exercises proposed for the alternative (Maven) have been documented above and many
commits can be found in Bitbucket, referring issue #13 - _CA2 - Part 2 - Write Technical Report_.

17. A final tag will be done in the VCS after the upcoming analysis "Gradle vs Maven" is completed. 


###Alternative Analysis - Gradle vs Maven
Both Maven and Gradle are build tools, being Gradle considered as an evolution of Maven. As referenced in DevOps support material
(author: Professor Alexandre Bragança):
>Build tools are programs that automate the creation of executable applications from source code.
>Building incorporates compiling, linking and packaging the code into a usable or executable form.
>Basically build automation is the act of scripting or automating a wide variety of tasks that software developers do in their day-to-day activities.

**How does Maven compare to Gradle regarding build automation features?**
(the main sources of information used for this analysis are https://gradle.org/maven-vs-gradle/ and https://stackify.com/gradle-vs-maven/)

- **Performance** - The website below is clear about this topic. Gradle performs better than Maven in multiple scenarios: large monolithic applications, 
 large multi-project build, medium multi-project build, small multi-project build.
https://gradle.org/gradle-vs-maven-performance/
This is attained, because gradle allows for **incremental builds**, which means, it checks which tasks need to be updated and 
which ones don't, providing a shorter build time.

- **Flexibility** - _Maven provides a very rigid model that makes customization tedious and sometimes impossible._ 
While Maven's rigid structure may make it easier to understand, it also makes it unsuitable for special requirements or specific 
automation problems. Gradle, on the other hand, is built to tackle that necessity for flexibility.
Creating a highly customized build might be a nightmare for Maven users. The POM file can easily get bloated as your project grows and 
might as well become an unreadable XML file.

- **User Experience** - Below you can find a good example of how Gradle can be less verbose than Maven, when trying to implement 
a simple operation.
>GRADLE (using Groovy)
![](images/Screenshot 2022-04-10 at 15.40.22.png)

> Maven (using XML)
![](images/Screenshot 2022-04-10 at 15.40.52.png) (this code is not complete... it continues!)

Gradle includes a rich build description language (DSL) based on Groovy, which facilitates dependency-based programming, 
multi-project builds, dynamic tasks and task rules. Most of these are very hard to implement using Maven, as explained before. 

- **Dependency Management** - _Both build systems provide built-in capability to resolve dependencies from configurable repositories. Both are able 
to cache dependencies locally and download them in parallel._
However, Gradle provides customizable dependency selection and substitution rules which enable the build of multiple 
source projects together, the creation of composite builds and faster and better-modeled builds.
Maven, on the other hand, has few built-in dependency scopes, which forces awkward module architectures in common scenarios 
like code generation.

- **Main commands overview** - (source: https://www.journaldev.com/8396/gradle-vs-maven)
![](images/Screenshot 2022-04-10 at 15.36.24.png)

  
**How can Maven be extended with new functionalities (i.e., new plugins or new tasks)?**
- **PLUGINS**
As described in the previous topic of this document ("Alternative Implementation"), plugins were used to customize Maven's features
and to implement the requested exercise. This was quite easy to do, because there were already plugins available for actions
such as copying or deleting files upon certain build phases. The only action needed was to copy those plugins into the `pom.xml`.
For regular customizations, such as the ones above, Maven supports a growing number of plugins now. However, if something 
more complex or customized would be demanded, the implementation would have revealed itself
more complicated than when using Gradle, which is made for build customizations, as explained and shown before.

- **TASKS**
When observing both maven’s pom.xml and gradle’s build.gradle file:
![](images/Screenshot 2022-04-10 at 15.36.15.png)
  (source: https://www.journaldev.com/8396/gradle-vs-maven)

As far as execution models are concerned, both have task groups and descriptions and both have a GroupID-ArtifactID-Version (GAV) structure.
However, Gradle is based on task dependencies, while Maven is based on a linear model of phases. With Maven, goals are attached to project phases,
and goals can be seen as the equivalent to Gradle’s tasks. Multiple Maven goals take on the form of an ordered list.
On the other hand, Gradle allows task exclusions, task ordering, among others.


**Another alternative to the approach presented**
There seems to be a good alternative on the matter of converting a Maven project into a Gradle project, as explained at:
https://www.vogella.com/tutorials/GradleTutorial/article.html#gradle_convert_maven (point 23).
The command `gradle init --type pom` ...

>_(...) can be used to convert an Apache Maven build to a Gradle build. This works by converting the POM to one 
or more Gradle files. It is only able to be used if there is a valid “pom.xml” file in the directory that the init task 
is invoked in (...)._

