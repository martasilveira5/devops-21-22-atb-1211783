# Class Assignment 3

## Part II
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 3 is divided in part I and II and so there are two folders,
one for each part respectively, available at the root of this repository (ca3Part1 and ca3Part2).

In preparation for the exercises, two issues were created in Bitbucket - "#16: CA3 - Part 2 - Write technical report" and 
"#17: CA3 - Part 2 - Implement proposed exercises".
This document will focus first on the activities proposed in the lecture slides, and it will clearly point out the differences
of implementing a solution with Parallels instead of Virtual Box (due to the host machine characteristics).
Later, it will describe the steps followed to resolve CA3Part2 proposed exercises. 

Vagrant is a tool that allows replicating specific VM configurations in several host machines and to share those configurations, using a Vagrant file. 

0.
`bew install vagrant`

1. It was confirmed that the installation happened successfully:

![](images/Screenshot 2022-04-27 at 10.19.14.png)

With the current directory on ca3Part2 folder:
`mkdir vagrant-project-1`
`cd vagrant-project-1`

2. With the cd on the created folder, vagrant is initialized and the box (VM) to be used is specified.
Note that the VM box being used is not the same as indicated by the exercise requirements, because the host machine being 
used to resolve this exercise is a MacBook Pro with Apple Silicon M1, and the suitable hypervisor is Parallels (as explained in part 1)
with an Ubuntu 20.04.02 ARM64. Therefore, we had to search for an adequate box on vagrant website, 
and we decided to go for: https://app.vagrantup.com/luminositylabsllc/boxes/ubuntu-20.04-arm64

`vagrant init luminositylabsllc/ubuntu-20.04-arm64`
![](images/Screenshot 2022-04-27 at 11.01.02.png)

The file created contains the information needed (infrastructure as a code) to start the VM.
It can be shared for example using a VCS repository so that other users can start the same VM.
![](images/Screenshot 2022-04-27 at 10.27.05.png)

3. After trying to run `vagrant up`
![](images/Screenshot 2022-04-27 at 10.46.45.png)

The warning mentions that Parrallels is not configured for this environment. 
As recommended in https://kb.parallels.com/en/122843?_ga=2.242208194.1104751421.1619471414-182379460.1618566929
`vagrant plugin install vagrant-parallels`

![](images/Screenshot 2022-04-27 at 10.53.02.png)

After trying to run `vagrant up` again, this has booted the virtualization. It created a virtual environment as described in
the VagrantFile, and it did the "provisioning", which contemplates:
- The needed setup to boot the VM. 
- Downloading the Vagrant box.
- Calling the hypervisor (Parallels) to create a VM from that box.
- Installing hypervisor tools as needed.
- Setup the network and shared folders.
- Running provision scripts to prepare the VM.
- Setup SSH keys to log on to the VM:
![](images/Screenshot 2022-04-27 at 11.02.30.png)

If we open Pararllels Desktop Control Center, it is possible to see that the recently created VM is running:
![](images/Screenshot 2022-04-27 at 11.05.06.png)
(_vagrant-project-1_)

Without having to open Parallels, it is also possible to check the machine status by running:
`vagrant status`
![](images/Screenshot 2022-04-27 at 11.06.52.png)
(_We see that the machine is running._)

Vagrant will do a series of setups that we had to do manually on CA3 part1, namely the ssh configuration. 
No password nor IP will be requested to boot ssh:
`vagrant ssh`

![](images/Screenshot 2022-04-27 at 11.11.31.png)

While using the remote terminal of the VM, it is possible to verify that the vagrantFile can be found on our VM running, too.
![](images/Screenshot 2022-04-27 at 11.22.58.png)

All VM configurations must be done on this VagrantFile, so as not to lose them. If we do the configurations for example directly
on Parallels settings, it won't be possible to share them later with someone else who receives the Vagrant file. 


Let's now clone the atb/vagrant-basic-example project to test using a previously created and configured VM.
First we need to exit the remote terminal ...
`exit`

Terminate the VM ...
`vagrant halt`

And clone the repo ...
`git clone https://github.com/atb/vagrant-basic-example.git`

In this example, the VagrantFile configures the installation of Apache, i.e., installs Ubuntu and then Apache. 
This will be done every time the VM is boot, thanks to the script below:
![](images/Screenshot 2022-04-27 at 11.36.31.png)

The vagrant file contains other configurations related with network, mapping virtual machine ports and physical machine ports,
configuring the IP addresses, shared folders, etc. It is not possible to run the machine without these configurations.

However, since we are using Parallels, some changes have to be done to the Vagrant File, as follows:

Change the cd to the folder of this new example
`cd ./vagrant-basic-example`

This project is configured for a VM using VirtualBox. Thus, the Vagrantfile will be edited to reflect configurations that can be run
on Parallels, using nano:
`nano Vagrantfile`

Edited the IP to match the network being used in the previous VM - the previous one was using node 10, so we changed this one to 11:
`config.vm.network "private_network", ip: "192.168.33.11"`
Changed the box to one that is compatible with Parallels/Ubuntu 20.04/Arm64/Apple Silicon M1:
`config.vm.box = "luminositylabsllc/ubuntu-20.04-arm64"`

`CTRL+O`
`CTRL+X`

Started the VM
`vagrant up`

![](images/Screenshot 2022-04-28 at 12.28.07.png)
(_The VM booted successfully_)

Now that Apache is installed during provisioning, we should be able to use the browser in the host machine (as specified in
VagrantFile) to run the application:
`localhost:8010`

![](images/Screenshot 2022-04-28 at 12.31.25.png)

If we edit the HTML file and add "MARTA", it shall replicate the changes

![](images/Screenshot 2022-04-28 at 12.33.20.png)

One important note is that the provisioning only happens at the first time the machine is booted.
If we change something in the provisioning, the next time the machine is booted,
we have to run the option `vagrant reload --provision` to apply those changes. This command will be used several times in
the upcoming exercises. 

In this case, this command was not needed and `vagrant up` was enough, because the machine had not been booted before. 

The configurations requested in the lecture slides are already done in the VagrantFile of professor's project:
- Shared folder
- Port forward to localhost, port 80 -> 8010: http://127.0.0.1:8010 
- The VM on a static IP of a host-only network: http://192.168.56.5 - in this case it was used 192.168.33.11
- Make the VM webserver serve pages from a shared folder - The default location of the webpages in apache is /var/www/html


Before entering the exercises proposed in CA3Part2 Requirements, we will commit the work done so far. 
For that purpose, I will first remove the .git folder which came from the cloning of professor's project "vagrant-basic-example"
`git add .`
`git commit -a -m "Addresses #17. Add more details for readme.md for CA3Part2 and adds vagrant-basic-example from professor's lecture.`



####Implementation of CA3PART2 exercises

In professional contexts, its is a common approach to have to configure several VMs using Vagrant. 
We attempted such, using one VM for the database and another for the web application in REACT/Java.

1) We will use professor's project: atb/vagrant-multi-spring-tut-demo.
2) ---
3) Placed the cd on folder ca3Part2 and cloned the repository.
`mkdir vagrant-project-2`
`cd ./vagrant-project-2`
`git clone https://bitbucket.org/atb/vagrant-multi-spring-tut-demo.git`
`cd ./vagrant-multi-spring-tut-demo`
`nano Vagrantfile`

4) As done for the previous projects, the VagrantFile will be edited, so that the vagrant box used becomes luminositylabsllc/ubuntu-20.04-arm64.
This configuration will be changed for both VMs.
![](images/Screenshot 2022-04-28 at 14.45.37.png)

Let's change also the IP for the next sequential number of the previously used network: 12 and 13 for each VM
![](images/Screenshot 2022-04-28 at 14.46.38.png)

Define 1 GB of RAM. For this we had to change the Vagrantfile to include the appropriate commands for parallels to configure the RAM.
![](images/Screenshot 2022-04-28 at 14.48.10.png)

The scripts for the installation of git, node.js and Tomcat - JAVA webserver - were not changed at this point.

The git clone command was changed to go get my own repository and the cd command was pointed to the folder in the repository that contains 
the ca2Part2 exercise (the one using Gradle).
![](images/Screenshot 2022-04-28 at 14.51.11.png)

If all goes well, the folder `/build/libs` will contain a `.war` file that will be copied over to the TomCat folder. 
This will be a sort of a deployment for a production environment. 

After doing all these configurations we can do `vagrant up`.
The terminal says that has no access to Bitbucket...
![](images/Screenshot 2022-04-28 at 15.15.42.png)

So we are making the Bitbucket repository public as recommended in professor's slides...
![](images/Screenshot 2022-04-28 at 15.15.14.png)

...and tried vagrant up again with reload
`vagrant reload --provision`

This time the TomCat file was not found...
![](images/Screenshot 2022-04-28 at 15.15.42.png)

After entering the remote terminal of the VM and checking the TomCat version being used by the VM, we realized it was version 9 instead of 8
...so we changed the TomCat version to 9 in the script:
![](images/Screenshot 2022-04-28 at 15.27.56.png)

Also, it was noticed the target cd was wrong, and it was fixed to:
![](images/Screenshot 2022-04-28 at 15.35.19.png)

We also noticed that we had to change the JDK version to 11, to match the content of gradle project used in CA2Part2:
`nano Vagrantfile`
![](images/Screenshot 2022-04-28 at 15.49.48.png)
`CTRL + O`
`CTRL + X`

...and tried vagrant up again with reload
`vagrant reload --provision`

The machine started, but displayed an error on the task ":installFrontend":
![](images/Screenshot 2022-04-28 at 23.46.20.png)

After checking in with the professor, we confirmed that the folder `node` and `node_modules` were not being committed to the 
repository, thus they could not be the root cause of this error.
So, we had to do the same fix as done for CA3Part1, which is to change the frontend task used in CA2Part2 in a way that it uses
a node.js version that is "consumable" by Ubuntu 20.04.
```
frontend {
nodeDistributionProvided=false
nodeVersion='14.17.3'
nodeDistributionUrlRoot='https://nodejs.org/dist/'
nodeDistributionUrlPathPattern='vVERSION/node-vVERSION-ARCH.TYPE'
nodeDistributionServerUsername='username'
nodeDistributionServerPassword='password'
nodeInstallDirectory=file("${projectDir}/node")
}
```

At this point, the changes were commit to the repository:
`git add .`
`git commit -a -m Addresses #17. Update readme.md file and build.gradle file to refactor the frontend task, to make it compatible with Linux Arm64`

...and tried vagrant up again with reload
`vagrant reload --provision`

It still didn't work:
![](images/Screenshot 2022-04-29 at 00.04.35.png)


This is expected because during the build of ca2Part2, jar files were generated instead of war files, which is the format expected 
considering the configurations specified in the vagrantFile.
Therefore, the changes done in professor's repository https://bitbucket.org/atb/vagrant-multi-spring-tut-demo.git were replicated
to the CA2PART2 exercise files and the vagrantFile had to be edited to change the name of the war file being generated to:
`./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war`.

The name of this `.war` file had to be changed on another places such as app.js, for this purposed the function `find in files`
from IntelliJ was used to help out the refactoring.

...At this point the changes were commited to the repository. 
`git add.`
`git commit -a -m "Addresses #17. Made needed configurations to gradle project and done in professors repository named tut-basic-gradle."`
`git push origin main`

...And tried vagrant up again with reload
`vagrant reload --provision`

The previously mentioned error persisted...
![](images/Screenshot 2022-04-29 at 00.50.51.png)

...and it was noticed that one of the lines of the VagrantFile still contained the wrong TomCat version. This was fixed as follows:
`sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat9/webapps`

After that, the error persisted, so we changed the `.gitignore`file to match the professor's and made a commit:
`git add.`
`git commit -a -m "Addresses #17. Made needed configurations to gradle project as done in professors repository named tut-basic-gradle."`
`git push origin main`

...And tried vagrant up again with reload
`vagrant reload --provision`

Since none of the previous changes worked, the VM was deleted and all the previous steps were repeated.
We noticed that the file `application.properties` file from c2part2 was missing the right file name...
`server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT`

After these changes, the task `:installFrontend` was still failing, so we deleted some tasks added as part of the CA2PART2 exercise to the `build.gradle` file. 

As this still didn't work, we forced the script contained in the VagrantFile to install a specific node.js and JDK version.
This was needed, because we noticed that the VM was not installing by default the versions to be used, as specified in the `:installFrontend` task.

```
sudo apt-get install openjdk-11-jdk -y
sudo apt-get install nodejs v14.17.3 -y
```

This still didn't work. Using the command line from the VM `web`, we tried to understand what node version was being installed. 
It was not the desired one (14.17.3), but instead it was v.12.22.
Thus, the vagrant file was edited again to force the installation of a version that was equal to or higher than 16.0.0, which is 
the one that is compatible with Ubuntu 20.04 for Arm64 architecture:
```
curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt -y install nodejs
```

This fix didn't resolve the issue, so we added this line to the script, before the `./gradlew clean`:
`chmod u+x webpack.config.js`

It didn't work, and an error related with the SpringBoot plugin showed up:
![](images/Screenshot 2022-04-29 at 13.00.03.png)

We edited the file `build.gradle` to try to fix this issue:
```
plugins {
id 'org.springframework.boot' version '2.6.7'
id 'io.spring.dependency-management' version '1.0.11.RELEASE'
id 'java'
id "org.siouan.frontend-jdk11" version "6.0.0"
id 'war'
}
```

This change didn't work and the error on the task `:installFrontend` returned.
So I tried to check which node.js version was being installed. It was `nodeVersion='v16.15.0'` so the `installFrontend`
task was changed accordingly.
```
frontend {
nodeDistributionProvided=false
nodeVersion='v16.15.0'
nodeDistributionUrlRoot='https://nodejs.org/dist/'
nodeDistributionUrlPathPattern='vVERSION/node-vVERSION-ARCH.TYPE'
nodeDistributionServerUsername='username'
nodeDistributionServerPassword='password'
nodeInstallDirectory=file("${projectDir}/node")
assembleScript = 'run webpack'
}
```

The frontend error was solved, but then we received an error saying that SSH was not connecting.
We decided to roll back the IP address to the one suggested by the professor. This change was done
in the `application.properties` file and on the vagrant file.

This solved the SSH issue, but another error appeared: `web: Execution failed for task ':installNode'.` 
We tried to change the sioguan plugin to version 5.1.0 as recommended in https://github.com/siouan/frontend-gradle-plugin/issues/148.

At this point, and considering that the machine still didn't work, we decided to start again.


####Implementation of CA3PART2 exercises - second attempt

Vagrant was uninstalled: `brew uninstall vagrant`.

The VMs that had been created and their files were deleted. 

Considering that we wanted to get back to the repository state where none of the previous changes had been done to CA2PART2 files, we will create 
a new branch at commit `167d5ff` and work from there. 
```
git branch ca3part2 167d5ff278b314ad3740928112ac6ffdc0a8ab7a
git push origin ca3part2
git checkout ca3part2
```

5) To avoid potential errors that could rise due to the usage of my own exercise resolution of ca2Part2, and before trying the `vagrant up`, 
we will clone the `tut-basic-gradle` repository owned by the professor to the folder CA3Part2.
`mkdir tut-basic-gradle`
`cd tut-basic-gradle`
`git clone https://bitbucket.org/atb/tut-basic-gradle.git`
And finally deleted the `.git` file from this folder, to avoid problems with the VCS.

Besides, the Vagrantfile will be updated to include all changes described in the previous steps of this document, except for...
- We will use the box `luminositylabsllc/bento-ubuntu-20.04-arm64` instead;
- This project `tut-basic-gradle`, owner by the professor, will be used instead of my own exercise resolution of ca2Part2, as explained before.
- The package net-tools will be installed, so that we can run `ifconfig` on the VM, if needed.
- Considering that we are currently using branches in our repository, some commands were introduced in the Vagrant File script
to make sure it is using the files from the branch "ca3part2", not the ones from the main branch.

![](images/Screenshot 2022-05-01 at 12.44.49.png)
![](images/Screenshot 2022-05-01 at 12.45.00.png)


After these changes, we tried `vagrant up`, but the following error was shown:

![](images/Screenshot 2022-05-01 at 12.56.54.png)

So, the vagrant file was changed to the following...
```
git clone https://martasilveira5@bitbucket.org/martasilveira5/devops-21-22-atb-1211783.git
cd devops-21-22-atb-1211783
git checkout ca3part2
cd ./ca3Part2/tut-basic-gradle/tut-basic-gradle
chmod u+x gradlew
./gradlew clean build
```

... and ran
`vagrant reload —provision`

The following error was expected... 
![](images/Screenshot 2022-05-01 at 13.01.26.png)

... we know that Linux Arm64 requires a specific plugin and node versions, as explained in:
https://github.com/siouan/frontend-gradle-plugin/issues/148.
According to this forum, Linux Arm 64 requires at least node.js 16.0.0 and siouan plugin 5.1.0. Thus, the following changes were
done to the `build.gradle` file:
![](images/Screenshot 2022-05-01 at 13.37.05.png)

We also noticed that Linux is installing by default the node.js version 10.19.0, which is not enough. 
![](images/Screenshot 2022-05-01 at 13.34.56.png)

So we had to force the installation of 16.0.0 on the vagrant file:
```
cd ~
curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt install nodejs -y
```
...
and the VM was destroyed and a new one initiated by running `vagrant up`

This time, the terminal complained that the version used for siouan was not found. 
![](images/Screenshot 2022-05-01 at 13.57.59.png)
According to https://plugins.gradle.org/plugin/org.siouan.frontend-jdk11, the plugin was changed to:
`id "org.siouan.frontend-jdk11" version "6.0.0"`

However, this time we got another error:
![](images/Screenshot 2022-05-01 at 14.20.58.png)

As suggested in https://discuss.gradle.org/t/caused-by-java-lang-nosuchmethoderror-org-gradle-api-provider-provider-foruseatconfigurationtime/36626,
we decided to update the `gradle-wrapper.properties file in folder tut-basic-gradle to a more recent gradle version that would be 
compatible with Linux Arm 64 and the node.js version previously installed. 
`distributionUrl=https\://services.gradle.org/distributions/gradle-7.4.2-bin.zip`

...and the VM was destroyed and a new one initiated by running `vagrant up`

The following error showed up:
![](images/Screenshot 2022-05-01 at 14.33.27.png)

Considering this, the task "frontend" was updated on the file `build.gradle`, to specify the node version that matches the one that Linux
has currently installed: 
![](images/Screenshot 2022-05-01 at 14.34.41.png)

```
frontend {
nodeVersion = '16.15.0'
// See 'scripts' section in your 'package.json file'
//cleanScript = 'run clean'
//assembleScript = 'run assemble'
assembleScript = 'run webpack'
//checkScript = 'run check'
}
```

...and the VM was destroyed and a new one initiated by running `vagrant up`

This didn't work and raised the following issue:
![](images/Screenshot 2022-05-01 at 14.55.17.png)

So, I decided to use a gradle version above 6.5 (as recommended) but not as recent as 7.42.
Edited the `gradle-wrapper.properties file:
`distributionUrl=https\://services.gradle.org/distributions/gradle-6.5.1-bin.zip`

... the VM was destroyed and a new one initiated by running `vagrant up`

Didn't work:
![](images/Screenshot 2022-05-01 at 15.23.59.png)
After some research about `ELF NOT FOUND`, I tried to delete the line `sudo ln -s (...)` from the vagrant file.
(_https://stackoverflow.com/questions/20886217/browserify-error-usr-bin-env-node-no-such-file-or-directory/20890795#20890795_)

... the VM was destroyed and a new one initiated by running `vagrant up`

Didn't work:
![](images/Screenshot 2022-05-01 at 17.25.08.png)
So, we tried to add the recommended "frontend" task code on https://siouan.github.io/frontend-gradle-plugin/configuration/:
```
    nodeDistributionProvided = false
    nodeVersion = '16.15.0'
    nodeDistributionUrlRoot = 'https://nodejs.org/dist/'
    nodeDistributionUrlPathPattern = 'vVERSION/node-vVERSION-ARCH.TYPE'
    nodeDistributionServerUsername = 'username'
    nodeDistributionServerPassword = 'password'
    nodeInstallDirectory = file("${projectDir}/node")
```

... the VM was destroyed and a new one initiated by running `vagrant up`

After several other attempts, the final solution that attained good results was to use the following "frontend" task in the `build.gradle` file:
```
frontend {
	nodeDistributionProvided = true
	nodeVersion = '16.15.0'
	nodeInstallDirectory = file("/usr")
	assembleScript = 'run webpack'
}
```
This way, the node version being used is the one installed automatically by Ubuntu in the location `/usr/bin/node`.

We also had to change the vagrantFile, because we were using the tut-basic-gradle project:
`sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat9/webapps`

And finally it worked!
![](images/Screenshot 2022-05-01 at 19.34.13.png)

All 4 links recommended in the readMe file of the vagrant-multi-spring-tut-demo project were successfully tested:

_http://localhost:8080/basic-0.0.1-SNAPSHOT/_
![](images/Screenshot 2022-05-01 at 19.25.47.png)

_http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/_
![](images/Screenshot 2022-05-01 at 19.24.38.png)

_http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console/login.jsp?jsessionid=eb9b3a417a4a39c4ebcd5e8b61f89e39_
![](images/Screenshot 2022-05-01 at 19.25.01.png)

_http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console/login.jsp?jsessionid=b2c6287e91332a95b39d846f83a71f6e_
![](images/Screenshot 2022-05-01 at 19.25.15.png)


To test the database, it was used the URL `jdbc:h2:tcp://192.168.56.11:9092/./jpadb` as recommended in the readMe file. 
This URL includes the IP of the VM named "db" and the configured port, which is 9092.
Clicked on "Connect":

![](images/Screenshot 2022-05-01 at 19.37.10.png)

When trying to do the query "SELECT * FROM EMPLOYEE", it worked as expected:
![](images/Screenshot 2022-05-01 at 19.38.36.png)

When trying to insert a new record to the database, it worked as expected too:
![](images/Screenshot 2022-05-01 at 19.40.22.png)

And finally the frontend was tested, as it should show the new register named "Marta Silveira":
![](images/Screenshot 2022-05-01 at 19.41.19.png)

6) The steps followed to resolve this class assignment were described in detail in this document, as well as all the 
errors and problems found along the way, together with the appropriate solutions.

7) Before tagging the repo, the merge between the main branch and the branch created for this exercise was done. 
Please note that, to make the multi VM project work, the vagrant file used for CA3Part2 would have to be reconfigured after this merge, 
because it was configured to work with the code being developed in ca3Part2 branch, not on main branch, as shown below.
In fact, the only change needed would be to remove line 3 below from the vagrant file:
```
1. git clone https://martasilveira5@bitbucket.org/martasilveira5/devops-21-22-atb-1211783.git
2. cd devops-21-22-atb-1211783
3. git checkout ca3part2
4. cd ./ca3Part2/tut-basic-gradle/tut-basic-gradle
```

Considering that we wanted to overwrite all the files from the branch "main" with the files from the branch "ca3Part2", 
we followed the steps described in: https://gist.github.com/ummahusla/8ccfdae6fbbe50171d77

```
git checkout ca3Part2
git merge -s ours main
git checkout main
git merge ca3Part2
git push
```

Finally, the Bitbucket issues were closed and the repository was marked with a tag:
```
git tag ca3-part2
git push origin ca3-part2
```

###Analysis - how does Parallels compare with Virtual Box?

####How does Parallels compare to VirtualBox regarding virtualization features?

*Features*
![](images/Screenshot 2022-05-02 at 13.59.03.png)
(taken from https://www.parallels.com/eu/pd/virtualbox/?gclid=EAIaIQobChMIj-Lpyc_A9wIV2Y9oCR0jGwIAEAAYASAAEgIl0fD_BwE)

* Both VirtualBox and Parallels are used to run multiple operating systems, whether they be the same as
the host operating system, or different. However, Parallels seems to be more popular amongst MacOS users.
(source: https://www.trustradius.com/compare-products/oracle-vm-virtualbox-vs-parallels-desktop)

* Oracle VM VirtualBox offers a high level of customization, allowing users to dictate exactly what hardware configurations they need.
On the other hand, Parallels offers high CPU and GPU performance, making it a good choice for businesses that want to run 
demanding applications on virtual machines.
(source: https://www.trustradius.com/compare-products/oracle-vm-virtualbox-vs-parallels-desktop)

* One point that favours VirtualBox, specifically when it comes to Vagrant users, is that the Vagrant Cloud (https://app.vagrantup.com/boxes/search?page=6&provider=virtualbox)
seems to have way more options for VirtualBox than for Parallels. In other words, most boxes are suitable for VirtualBox, but
not all of them are suitable for Parallels. This seems to be a direct consequence of VirtualBox being a more popular provider. 

*Limitations*
* Given the experience of resolving ca3Part2, Parallels has not shown any limitation and it revealed to be quite easy to use.
This is supported by the table above, which clearly shows that Parallels does not stay behind Virtual Box when it comes to the main
features.
 
* Getting a good performance out of Oracle VM VirtualBox _requires technical ability, and it still may not be suitable for virtual 
machines running high-demand applications._
(source: https://www.trustradius.com/compare-products/oracle-vm-virtualbox-vs-parallels-desktop)

* Parallels Desktop is simple to use and walks users through the virtualization process, but it doesn’t provide the same 
level of customization as Oracle VM VirtualBox. 

*Pricing*
* Oracle VM VirtualBox is a free to use technology for individual users.
Parallels Desktop is not free, however there is also a free trial available for a limited time. 


#### How can Parallels be used with vagrant to solve the same goals as presented for this assignment?

It is important to note that due to the host machine characteristics (Macbook Pro with Apple Silicon M1), the requirements of the
main exercise could not be fully implemented. Instead, it was implemented an alternative using Parallels as a provider instead of
Virtual Box. During the course of this exercise implementation it was noticed that using Parallels on an Apple Silicon M1, would
require the following software to accomplish the required steps:
- The installation of the vagrant-parallels plugin.
- A box compatible with the architecture ARM64, which would have to be Ubuntu 20.04 ARM64, which in its turn demanded:
  - TomCat version 9.
  - JDK 11.
  - Node.js 16.0.0 or higher.
  - Gradle 6.5.0 or higher.
  - The plugin org.siouan.frontend-jdk11 version 6.0.0 or higher.

It is also important to note that several attempts were done using my own resolution of ca2part2 (Gradle implementation).
However, after several failures which could not be solved, it was decided to copy the whole professor's project named "tut-basic-gradle"
into my own Bitbucket repository, and to use it to replace my own the resolution of ca2Part2. This made sure all the configurations
needed were successfully implemented, and it turned out to be a good solution.

Besides the items mentioned above, Parallels satisfied the needs of ca3Part2 quite well. The main limitation revealed to be, 
in fact, the architecture of the processor of the host machine. 
