# Class Assignment 5

## Part II
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783

<ins>Repository structure</ins>: The class assignment number 5 is divided in part I and II and so there are two folders,
one for each part respectively, available at the root of this repository (ca5Part1 and ca5Part2).

In preparation for the exercises, two issues were created in Bitbucket - "#24: CA5 - Part 2 - Write technical report" and
"#25: CA5 - Part 2 - Implement proposed exercises".

#### Lecture activities

This time, we will not replicate the lecture activities, because the host machine being used to resolve this class assignment 
is a MacBookPro with Apple Silicon M1. To use Vagrant on such machine, a provider would be needed, however, VirtualBox is not 
compatible with this machine and the software Parallels used as a replacement in Ca3Part2 has expired its license by this moment. 

Having said so, we will skip the lecture exercises and dive into the class assignment. 

#### Class assignment

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the REACT and Spring application - 
gradle basic version - developed in CA2 Part2 and explored into more detail during CA3 Part 2 (already with the usage of `.war` files). 

For that purpose, I have copied the folder `tut-basic-gradle` on my repository under `ca3Part2/tut-basic-gradle` into CA5Part2 folder.
We did a copy of the jenkins file created for CA5Part1 and placed it on CA5Part2 folder too. This file was adapted as described below:
- Kept the `Checkout` stage unchanged.
- Kept the `Assemble` stage - we just had to change the dir to `ca5Part2/tut-basic-gradle`.
- Kept the `Test` stage and changed the following: 
```
sh './gradlew test'
junit 'build/test-results/test/*.xml'
```

This will run the tests using the gradle task `test` and then it will use the command `junit` to store
the results in the indicated path. `Junit` is in fact a plugin that Jenkins uses to _provide useful information about test results, 
such as historical test result trends, a web UI for viewing test reports, tracking failures, and so on._ (source: https://www.jenkins.io/doc/pipeline/steps/junit/)

- Added a new stage called `Javadoc`. To implement this task, we followed the steps described at https://plugins.jenkins.io/htmlpublisher/ 
and https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/. First of all, we placed the current directory on `ca5Part2/tut-basic-gradle` 
and ran `./gradlew tasks` to understand if gradle already contained a javadoc task. This was confirmed as shown below:

![](images/Screenshot 2022-06-02 at 15.47.01.png)

Then, we added a step to our stage to run `./gradlew javadoc` on directory `ca5Part2/tut-basic-gradle`. 
If we run this task directly on our command line, we see the following:

![](images/Screenshot 2022-06-02 at 15.49.47.png)

This way, we see that the output of this task will be saved in the jenkins workspace under `build/docs/javadoc`.
So, as indicated in https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/, we added:
1.The directory where the report shall be generated, according to what was found above; 2. The name of the main html file `myreport.html`; 3. 
The name of the report and its title. 

Note that this `publishHTML` step is in fact a Jenkins plugin to _publish HTML reports that your build generates to the job and build pages._
(source: https://plugins.jenkins.io/htmlpublisher/)

- Kept the `Archiving` stage but changed the `archiveArtifacts` step to include all files under `build/libs/*`. In part 1 we were just copying 
a `.jar` file, but at this point we want to copy a `.war` file, so we had to change that piece of code. 

- Added a new stage called `Publish Image`. This stage will require the creation of a docker image based on a Dockerfile and the publishing 
of that same image into Docker Hub repository, under my account. To attain that, we will start by creating a Dockerfile, 
then use the command `docker.build` to create the image. Finally we will use the command `push()` 
to send it to Docker Hub. The tag for this new image will be the job build number of Jenkins, stored at the variable `env.BUILD_ID`:
`mvs5/imageca5part2:${env.BUILD_ID}`. 

The credentials used to do the publishing will be the ones configured at Jenkins, as the command `docker.withRegistry()` will get those.
To create the Docker Hub credentials at Jenkins we navigated to `Credentials > System > Global credentials` and then filled the form as follows:

![](images/Screenshot 2022-06-02 at 17.30.20.png)

It is very important that the `id` field is filled with the same keyword used in the Jenkins file under `dockerhub_credentials`:

`docker.withRegistry('', 'dockerhub_credentials'){`

It is also very important that both the Docker file and Jenkins file are located in the same directory and that this directory
is the root of the project. In other words, they must be placed in the same directory as the gradle files. 

More details on configuring this `Publish-image` stage can be found here: https://www.jenkins.io/doc/book/pipeline/docker/

Finally, after having the jenkins and docker files ready, we committed the changes described above to the repository. 
```
git add .
git commit -a -m ...
git push origin master
```

**Note**: the docker file is a very simple one, it imports an image with tomcat9 and jdk11 (to be compatible with the project tut-basic-gradle configurations)
and then adds the `.war` file from the jenkins workspace on my local machine into the docker container:
```
FROM tomcat:9.0-jdk11

ADD build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8081
```

Let's now configure the Jenkins pipeline to execute this project!
We created a new item on jenkins, and configured the pipeline as follows;

![](images/Screenshot 2022-06-02 at 17.59.33.png)

Before running this new item directly on Jenkins, we ran `./gradlew clean build` on our local machine first, to dismiss 
any bugs that could occur. The build failed because of `install frontend` task:

![](images/Screenshot 2022-06-02 at 17.01.01.png)

Thus, we had to edit this task and change it to the following:
```
frontend {
	nodeDistributionProvided = true
	nodeVersion = '18.1.0'
	nodeInstallDirectory = file("/opt/homebrew")
	assembleScript = 'run webpack'
}
```
This configuration ensures that the node version being used for the build is the one installed at my local machine. This is important
because the machine being used is a MacBookPro with Apple Silicon M1, and thus not all node versions are compatible with this architecture. 
To understand what node version was installed on my machine and where it was installed (to fill in the `nodeInstallDirectory` section
of the `frontend` task), we had to run the following commands first:
```
node -v
which node
```

![](images/Screenshot 2022-06-02 at 17.17.53.png)

After this, I ran `./gradlew clen build` again and the `.war` file was finally generated at `build/libs` with the name
`basic-0.0.1-SNAPSHOT.war`.
The changes were committed and pushed to the repository.

Now let's try to run the full pipeline configured on Jenkins for the first time - clicked on `Build now`.
The first build failed on the Test stage, because I had not added tests to the tut-basic-gradle project. 

![](images/Screenshot 2022-06-02 at 18.24.26.png)

I added a very simple test to the tut-basic-gradle project and committed and pushed the changes to the repository. 

![](images/Screenshot 2022-06-02 at 18.25.06.png)

Next, I tried `build now ` again.
This time, the build failed on the Javadoc stage, because I had not yet installed the plugin. 

![](images/Screenshot 2022-06-02 at 18.23.20.png)

So, we went to `Dashboard > Plugin manager`, searched for available Javadoc and HTML Publisher plugins and installed them.
At this point, Jenkins was restarted. 

We tried another build, but it failed again, this time at the `Publish-image` stage. 
I realized that I had not yet installed the plugin for Docker. Hence, I followed the same steps as described above for plugin installation. 
The plugin installed was named `docker pipeline`.

We tried another build, but it failed again. This time the error mentioned that Docker could not be found:

![](images/Screenshot 2022-06-02 at 23.23.20.png)

After some digging in the internet, we found this article:
https://stackoverflow.com/questions/50333325/jenkins-cannot-run-program-docker-error-2-no-such-file-or-directory

We tried several of the solutions found there, including configuring the Docker Installation as shown below:

![](images/Screenshot 2022-06-02 at 23.24.47.png)

None of them worked. After checking with the professor, he suggested that this could be due to an improper Jenkins installation.
In my case I had used Homebrew to install Jenkins. Considering that, I decided to uninstall Jenkins (`homebrew uninstall jenkins`)
and to install it again using the `.war` file at: https://www.jenkins.io/download/ (as suggested in professor's lecture slides).
I downloaded the file and ran the command `java -jar jenkins.war`. Jenkins started successfully.

I opened `localhost:8080` and tried to run build again - the previous issue was solved! 
Even though Jenkins generated the container, still the build failed on:

![](images/Screenshot 2022-06-02 at 23.30.50.png)

The war file could not be found. To fix this, we did the following correction on the Dockerfile:
```
FROM tomcat:9.0-jdk11
ADD build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
EXPOSE 8081
```

We replaced the `COPY` line by `ADD` line and the changes were committed and pushed to the repository.
This solved the issue! We tried the build again, but a new error popped in:

![](images/Screenshot 2022-06-03 at 15.03.24.png)

After some research at stackoverflow, we found the following interesting recommendation:
https://stackoverflow.com/questions/38276341/jenkins-ci-pipeline-scripts-not-permitted-to-use-method-groovy-lang-groovyobject

So we went to  `Jenkins > Manage jenkins > In-process Script Approval` and approved the pending process as shown below:

![](images/Screenshot 2022-06-03 at 14.59.55.png)

We tried the build again, but it still failed and showed the following error:

![](images/Screenshot 2022-06-03 at 15.45.31.png)

We realized this was due to missing parenthesis in the Jenkins file (stage `Publish-image`).
This was fixed and the changes were committed and pushed to the repository.

Finally all steps of the build ran successfully, however the pipeline still failed:

![](images/Screenshot 2022-06-03 at 15.46.44.png)

When we inspected the console output, we realized that during the Javadoc stage, Jenkins could not find the destination folder
where to place the reports:

![](images/Screenshot 2022-06-03 at 15.47.42.png)

We fixed the `reportdir` attribute as follows, to match the correct path for ca5Part2 folder, and the changes were 
committed and pushed to the repository.

![](images/Screenshot 2022-06-03 at 15.49.21.png)


The build terminated successfully!!

![](images/Screenshot 2022-06-03 at 15.58.13.png)

Let's now confirm that each step accomplished the desired intent:
- Checkout
  - The repository has been copied into Jenkins workspace:
  
![](images/Screenshot 2022-06-05 at 12.06.25.png)

- Assemble and archiving
    - The `.war` file was generated and archived:
  
![](images/Screenshot 2022-06-05 at 12.10.45.png)

- Test
  - The test results were published in `.xml` format and statistics were generated:
  
![](images/Screenshot 2022-06-05 at 12.12.03.png)
  
- Javadoc
  - The Javadoc html report was generated at `build/docs/javadoc`:
  
![](images/Screenshot 2022-06-05 at 12.13.16.png)

- Publish 
  - When checking Docker Hub and Docker Desktop, we see that the image has been successfully published:

![](images/Screenshot 2022-06-03 at 15.59.19.png)

![](images/Screenshot 2022-06-03 at 15.59.47.png)


## Alternative Exercise using Buddy Works
###Alternative Implementation

For the alternative exercise, we will use Buddy Works to create a pipeline to be integrated with Bitbucket. 

First of all, a new Bitbucket account was created with my personal e-mail, because for some unknown reason Buddy Works was not
accepting a BitBucket account associated to an ISEP e-mail. I created a new repository, which you can access via
`https://bitbucket.org/mvs5/devops-21-22-ca5part2-alt`. This repository was made public, nevertheless please contact me 
in case of trouble accessing it. 

This new repository was cloned to my machine `git clone`, and I copied the content of the `tut-basic-gradle` project used in 
Ca5Part2 to the root of this new repository.
I removed the Jenkins file from the folder, but left the Dockerfile, as it was still needed. 
Committed the changes and made the first push to remote.

Next, I created an account on Buddy works, by selecting the option `Bitbucket`.
Then a new project was created on Buddy and then I added a new Pipeline to it, as shown below. 

![](images/Screenshot 2022-06-04 at 23.24.31.png)

Named the pipeline DevOps and defined that the
trigger event for a new build would be a push to the master branch of the chosen Bitbucket repository.

Several actions were added to this pipeline:

1. Gradle build
2. Build docker image
3. Push docker image

![](images/Screenshot 2022-06-04 at 23.33.37.png)

Note that in the case of Buddy there is no need for a `Checkout` action, because when setting up the new project we are asked
to indicate the Git provider (in this case BitBucket) and the repository to be used.

![](images/Screenshot 2022-06-05 at 12.43.00.png)

On the contrary of Jenkins, where everything is configured using code, here we can just use the GUI to search for an action
(equivalent to a stage in Jenkins) from the available list and click on `+` to add it to the pipeline.
It is also possible to drag and drop actions to change their order. There are many pre-built actions that 
we can choose related with different frameworks: Gradle, Maven, AWS, Docker, etc. 

![](images/Screenshot 2022-06-05 at 12.45.09.png)

Let's now see into more detail how each action of our pipeline has been configured.

1. For the gradle build action, we just had to specify the gradle commands to be run:

![](images/Screenshot 2022-06-04 at 23.38.51.png)

(assemble, test and javadoc)

2. For the Docker build action, we just had to indicate the location of the Dockerfile:

![](images/Screenshot 2022-06-04 at 23.41.59.png)

3. For the push of the Docker image, we: (1) indicated that we wanted to publish an image generated on the previous action; (2) entered 
the details of my Docker hub account (username and password); (3) indicated the name of the repository at Docker Hub 
to be used; (4) defined a tag name. 

![](images/Screenshot 2022-06-04 at 23.42.56.png)

Having all these configurations ready, we clicked on `Run`, but the build failed. 

The first error thrown was about the Gradle version, which was not supported by the `siouand`plugin being used at the 
`build.gradle` file. On the configuration of this action, we changed the Gradle version being used to `6.5.1.-jdk11` which
we have known from the previous class assignments to be compatible with the project settings.

![](images/Screenshot 2022-06-04 at 23.40.20.png)

We tried to run the pipeline once more, but this time the `frontend` task failed.
This happened because during the Main Exercise implementation, we had to configure the `build.gradle` file in a way that it would run
in my local machine (with Apple Silicon M1). On the contrary of Jenkins,  Buddy Works does not run the pipeline
locally on my machine, so I had to take out the configurations I had done before and replaced them with the following:
```
frontend {
	nodeVersion = "14.17.3"
	assembleScript = "run webpack"
	cleanScript = "run clean"
	checkScript = "run check"
}
```

Afterwards, I committed and pushed the changes done to the repository and Buddy Works immediately initiated a new run.
This time, the build was successful! It was super fast and easy!

![](images/Screenshot 2022-06-04 at 23.44.42.png)


By accessing the file system section of Buddy Works and Docker Hub, we can confirm that each demanded step of the pipeline
has completed successfully:

-Assemble

![](images/Screenshot 2022-06-04 at 23.49.45.png)

-Test

![](images/Screenshot 2022-06-04 at 23.50.34.png)

-Javadoc

![](images/Screenshot 2022-06-04 at 23.51.37.png)

-Archive

![](images/Screenshot 2022-06-04 at 23.49.45.png)

-Publish 

![](images/Screenshot 2022-06-04 at 23.52.33.png)

![](images/Screenshot 2022-06-04 at 23.52.50.png)


To wrap-up the alternative exercise implementation, I would like to highlight that Buddy allows the export or import of `.yml` files,
in case you would not like to use the GUI.
For example, if we export the `.yml` file based on the configurations described in this document, we get the following:

```
- pipeline: "DevOps"
  on: "EVENT"
  events:
  - type: "PUSH"
    refs:
    - "refs/heads/master"
  priority: "NORMAL"
  fail_on_prepare_env_warning: true
  actions:
  - action: "Execute: gradle build"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-ca5part2-alt"
    docker_image_name: "library/gradle"
    docker_image_tag: "6.5.1-jdk11"
    execute_commands:
    - "gradle assemble"
    - "gradle test"
    - "gradle javadoc"
    - ""
    - ""
    - ""
    volume_mappings:
    - "/:/buddy/devops-21-22-ca5part2-alt"
    shell: "BASH"
  - action: "Build Docker image"
    type: "DOCKERFILE"
    dockerfile_path: "Dockerfile"
    target_platform: "linux/amd64"
  - action: "Push Docker image"
    type: "DOCKER_PUSH"
    docker_image_tag: "imageca5part2alternative"
    repository: "mvs5/imageca5part2alternative"
    integration_hash: "kyNzqmQO5xe7Ywqrb7rJX0A4o8"
```

If we consult BitBucket, we can also see a green arrow marking the build of the last commit as successful:

![](images/Screenshot 2022-06-04 at 23.54.50.png)


###Alternative Analysis

During the implementation of the alternative exercise it became clear that Jenkins and Buddy Works have different approaches
to CI/CD, but they also have some points in common:
1. On the contrary of Jenkins, no installation is necessary for Buddy Works. However, Jenkins is still open-source code,
while Buddy Works is paid (we have used a free trial version).
2. GUI seems to be the preferred mean of using Buddy, while Jenkins is mostly used with `.yml` files and plugins. Nevertheless,
both tools support GUI and `.yml` configurations. 
3. Buddy is a tool made for users who are not DevOps experts, as it facilitates the construction of a pipeline by 
having pre-built actions, whereas Jenkins is highly customizable, though it requires significant experience and knowledge of Groovy language. 
4. Buddy does not host the output of the pipeline. In other words, while Jenkins runs in the host machine and produces an output locally, 
Buddy Works operates fully online. 
5. Both tools support and operate with Bitbucket repositories, such as the ones used for the DevOps classes. 
6. Both tools allow the visualization of the build progress, which comes along with a console output - useful to 
understand what may have gone wrong.
 
There are other significant differences between these two options, the following tables summarize them well:

![](images/Screenshot 2022-06-05 at 13.14.11.png)

![](images/Screenshot 2022-06-05 at 13.24.14.png)

(source: https://hackernoon.com/buddy-vs-jenkins-mt4v32u2)

Some advantages of Buddy Works when compared to Jenkins:
- Buddy is a SaaS offering, meaning there is nothing to get running on our side. 
- DevOps has now joined the “as a Service” bandwagon. You can avoid the learning
  curve and time investment for wrangling your own CI/CD solution by simply paying the experts to do it for you.
  (source: https://medium.com/the-devops-corner/jenkins-and-buddy-c629d5d5e564) Using Buddy Works, any developer can create a complete process in 15 minutes, including
  build preparation, deployment, and system notifications.
- Buddy is a managed offering, meaning you don't need to actively maintain it.
- The dev environment remains the same for everybody on the team, 
doing away with it-works-for-me compatibility errors.

Some disadvantages of Buddy Works when compared to Jenkins:
- Jenkins has a broader approval, being mentioned in 1774 company stacks & 1526 developers
stacks; compared to Buddy, which is listed in 20 company stacks and 13 developer stacks.
(source: https://stackshare.io/stackups/buddy-vs-jenkins) This of course plays a role, because it means that it is very easy to
find support for Jenkins issues, but not that easy to do the same for Buddy Works. 
- Since Buddy is not as configurable as Jenkins and it relies on pre-built actions, it does not assure that you can build 
"anything" or "any project" using Buddy, whereas Jenkins does. 

In a nutshell, Buddy may be a good option in case there isn't a dedicated DevOps team with enough human resources to set up and 
maintain processes and where developers are in charge of those tasks. "Docker" is another reason why developers consider Buddy over the competitors, whereas "Hosted internally" 
was stated as the key factor in picking Jenkins.



#### Final Notes

After finishing this document, all issues were marked as resolved on Bitbucket, and the repository was tagged as follows:
```
git tag ca5-part2
git push origin ca5-part2
```