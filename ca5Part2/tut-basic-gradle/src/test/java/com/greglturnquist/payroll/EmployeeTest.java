package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void testEquals() {
        Employee employee1 = new Employee("marta", "Silveira", "desc");
        Employee employee2 = new Employee("marta", "Silveira", "desc");

        assertEquals(employee1, employee2);
    }
}