# Class Assignment 2

## Part I
###Exercise Implementation
####Analysis, design and implementation of the requirements

<ins>Link to the repository</ins>: https://bitbucket.org/martasilveira5/devops-21-22-atb-1211783/src/main/

<ins>Repository structure</ins>: Note that before implementing this exercise, the whole repository was restructured to move 
the contents of CA1 exercise to a dedicated ca1 folder. A commit on 31st March was done to resolve this issue (#9). 
The class assignment number 2 is divided in part I and II and so there are two folders, one for each part respectively, 
available at the root of this repository (ca2Part1 and ca2Part2).
The resolution of this first part is based on the repository from professor Luis Nogueira: gradle_basic_demo.
Its folder can be found under ca2Part1, together with this readme file. 

The upcoming steps were followed to implement exercises 1 to 7.

1. Created folders ca2-part1 and ca2-part2 on the root folder of the repository. 
The repository from professor Luís Nogueira was downloaded from Bitbucket directly to ca2-part1 folder. 
These steps were committed to the repository:
* `git add .`
* `git commit -a -m "Addresses issue #10, commit files from luisnogueira-gradle_basic_demo project"`
* `git push origin main`

2. The chat App was tested, by following the steps described on READMEadoc.md:
* `./gradlew build `
* `java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>`
* `./gradlew runClient`

![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Untitled%20picture.png)

The chat was also tested with multiple users (with VPN on). For that purpose, the task runClient on build.gradle was edited to have the same host 
on all users involved in the chat (IP address: 10.8.218.109). After that, it was possible to exchange messages.

![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Untitled%20picture_1.png)

3. Next, the task runServer was added to the file build.gradle. The type, group, description and classpath of this task are mostly 
similar to what is implemented on runClient. The main differences between these two tasks are:
* The main class referenced is 'basic_demo.ChatServerApp' instead.
* There is only one argument that references the port, because this is what is requested by the method "main" on class 
ChatServerApp (serverPort).

This task was run and tested. It is possible to verify that everything is working fine, when the server is running and
no exception is thrown. The confirmation message shall be "The chat server is running".
![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Untitled%20picture_2.png)

* `git add .`
* `git commit -a -m "Addresses issue #10, add task runServer to build.gradle"`
* `git push origin main`

4. First of all, the line `implementation'junit:junit:4.12'` was added to the file build.gradle to allow the implementation of tests.
Afterwards, a new directory was created: `src/test/java/basic_demo` and it was marked as a test directory. 
A new class called AppTest was added to it and the test `testAppHasAGreeting` was copied from Class Assignment 2 and pasted
to this new class. 
All tests were run and they successfully passed:
![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Untitled%20picture_3.png)

* `git add .`
* `git commit -a -m "Addresses issue #10, add new unit test and update gradle script"`
* `git push origin main`

5. Added a new task to build.gradle file based on the example from DevOps classes slides: 
```
taskcopySrc(type:Copy){
      from'src'
      into'backup'
}
  ```
When running the task, it was verified that it creates a new folder named `backup` in the root folder of the project (in case 
there isn't one yet) and that all content of the `src` folder is copied over (task type is "copy"). 
If this folder already exists, its content is replaced. 
![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Screenshot%202022-04-03%20at%2023.33.20.png)

* `git add .`
* `git commit -a -m "Addresses issue #10, add task to copy src folder into backup folder"`
* `git push origin main`

6. Added a new task to build.gradle file, it's type is "Zip":
```
taskzipSrc(type:Zip){
    archiveFileName = "src.zip"
    destinationDirectory = file(...project root pathname inserted here...)
    from "src"
}
```
This task creates a new file named `src.zip` in the root of the project (it is indicated by the property - destinationDirectory). 
It copies the contents of the src folder, as shown below:
![](luisnogueira-gradle_basic_demo-d8cc2d7443c5/images/Screenshot%202022-04-03%20at%2023.40.27.png)

* `git add .`
* `git commit -a -m "Addresses issue #10, add task to zip the contents of src"`
* `git push origin main`


After that, many commits followed to write this document. 

* `git tag ca2-part1`
* `git push ca2-part1`





